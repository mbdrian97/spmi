-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 06 Jan 2017 pada 18.24
-- Versi Server: 5.5.51-MariaDB-1~trusty
-- Versi PHP: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `unsri_kpi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_mengisi`
--

CREATE TABLE IF NOT EXISTS `admin_mengisi` (
  `id_mengisi` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) DEFAULT NULL,
  `id_jabatan_dosen` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_mengisi`),
  KEY `id_admin` (`id_admin`),
  KEY `id_jabatan_dosen` (`id_jabatan_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `atasan_bawahan`
--

CREATE TABLE IF NOT EXISTS `atasan_bawahan` (
  `id_atasan_bawahan` int(11) NOT NULL AUTO_INCREMENT,
  `id_dtt_atasan` int(11) NOT NULL,
  `id_dtt_bawahan` int(11) NOT NULL,
  PRIMARY KEY (`id_atasan_bawahan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data untuk tabel `atasan_bawahan`
--

INSERT INTO `atasan_bawahan` (`id_atasan_bawahan`, `id_dtt_atasan`, `id_dtt_bawahan`) VALUES
(1, 11, 12),
(2, 11, 13),
(3, 11, 14),
(4, 11, 15),
(5, 16, 17),
(6, 16, 18),
(7, 11, 16),
(8, 11, 19),
(9, 19, 20),
(10, 19, 21),
(11, 11, 22),
(12, 11, 23),
(13, 11, 24),
(14, 11, 25),
(15, 11, 26),
(16, 11, 27),
(17, 22, 28),
(18, 23, 29),
(19, 24, 30),
(20, 25, 31),
(21, 26, 32),
(22, 27, 33),
(23, 11, 34),
(24, 11, 38),
(25, 11, 42),
(26, 11, 46),
(27, 11, 50),
(28, 11, 54),
(29, 11, 58),
(30, 11, 62),
(31, 11, 66),
(32, 11, 70),
(33, 11, 74),
(34, 34, 35),
(35, 34, 36),
(36, 34, 37);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bobot_kategori`
--

CREATE TABLE IF NOT EXISTS `bobot_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `bobot` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kategori` (`id_kategori`,`id_jabatan`),
  KEY `id_jabatan` (`id_jabatan`),
  KEY `id_kategori_2` (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `bobot_kategori`
--

INSERT INTO `bobot_kategori` (`id`, `id_kategori`, `id_jabatan`, `bobot`) VALUES
(1, 3, 11, 0.35),
(2, 3, 12, 0.35),
(7, 3, 14, 0.35);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen_dtt`
--

CREATE TABLE IF NOT EXISTS `dosen_dtt` (
  `id_dtt` int(11) NOT NULL AUTO_INCREMENT,
  `id_dosen` int(11) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_dtt`),
  KEY `id_dosen` (`id_dosen`),
  KEY `id_jabatan` (`id_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kpi_dtt_tahunan`
--

CREATE TABLE IF NOT EXISTS `kpi_dtt_tahunan` (
  `id_kpi_dtt` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(11) NOT NULL,
  `id_kpi` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `baseline` decimal(10,0) NOT NULL,
  `capaian` decimal(10,0) NOT NULL,
  `target` decimal(10,0) NOT NULL,
  `bobot` smallint(6) NOT NULL,
  `rasio` decimal(10,0) NOT NULL,
  `persentase_capaian` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id_kpi_dtt`),
  KEY `id_jabatan` (`id_jabatan`),
  KEY `id_kpi` (`id_kpi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_admin`
--

CREATE TABLE IF NOT EXISTS `master_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `master_admin`
--

INSERT INTO `master_admin` (`id`, `username`, `password`, `name`) VALUES
(1, 'admin', 'admin12345', 'administrator kpi-r');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_dosen`
--

CREATE TABLE IF NOT EXISTS `master_dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `master_dosen`
--

INSERT INTO `master_dosen` (`id`, `nip`, `nama`) VALUES
(1, '19760425 201012 1 001', 'REZA FIRSANDAYA MALIK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_jabatan`
--

CREATE TABLE IF NOT EXISTS `master_jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi_jabatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=161 ;

--
-- Dumping data untuk tabel `master_jabatan`
--

INSERT INTO `master_jabatan` (`id_jabatan`, `deskripsi_jabatan`) VALUES
(11, 'Rektor'),
(12, 'Wakil Rektor 1'),
(13, 'Wakil Rektor 2'),
(14, 'Wakil Rektor 3'),
(15, 'Wakil Rektor 4'),
(16, 'Ketua LP3MP'),
(17, 'Wakil Ketua LP3MP'),
(18, 'Sekretaris LP3MP'),
(19, 'Ketua LP2M'),
(20, 'Wakil Ketua LP2M'),
(21, 'Sekretaris LP2M'),
(22, 'Ketua TIK'),
(23, 'Ketua PMS'),
(24, 'Ketua CDC'),
(25, 'Ketua Bahasa'),
(26, 'Ketua LAB'),
(27, 'Ketua Perpustakaan'),
(28, 'Sekretaris TIK'),
(29, 'Sekretaris PMS'),
(30, 'Sekretaris CDC'),
(31, 'Sekretaris Bahasa'),
(32, 'Sekretaris LAB'),
(33, 'Sekretaris Perpustakaan'),
(34, 'Dekan Fakultas Ekonomi'),
(35, 'Wakil Dekan 1 Fakultas Ekonomi'),
(36, 'Wakil Dekan 2 Fakultas Ekonomi'),
(37, 'Wakil Dekan 3 Fakultas Ekonomi'),
(38, 'Dekan Fakultas Hukum'),
(39, 'Wakil Dekan 1 Fakultas Hukum'),
(40, 'Wakil Dekan 2 Fakultas Hukum'),
(41, 'Wakil Dekan 3 Fakultas Hukum'),
(42, 'Dekan Fakultas Teknik'),
(43, 'Wakil Dekan 1 Fakultas Teknik'),
(44, 'Wakil Dekan 2 Fakultas Teknik'),
(45, 'Wakil Dekan 3 Fakultas Teknik'),
(46, 'Dekan Fakultas Kedokteran'),
(47, 'Wakil Dekan 1 Fakultas Kedokteran'),
(48, 'Wakil Dekan 2 Fakultas Kedokteran'),
(49, 'Wakil Dekan 3 Fakultas Kedokteran'),
(50, 'Dekan Fakultas Pertanian'),
(51, 'Wakil Dekan 1 Fakultas Pertanian'),
(52, 'Wakil Dekan 2 Fakultas Pertanian'),
(53, 'Wakil Dekan 3 Fakultas Pertanian'),
(54, 'Dekan FKIP'),
(55, 'Wakil Dekan 1 FKIP'),
(56, 'Wakil Dekan 2 FKIP'),
(57, 'Wakil Dekan 3 FKP'),
(58, 'Dekan FISIP'),
(59, 'Wakil Dekan 1 FISIP'),
(60, 'Wakil Dekan 2 FISIP'),
(61, 'Wakil Dekan 3 FISIP'),
(62, 'Dekan FMIPA'),
(63, 'Wakil Dekan 1 FMIPA'),
(64, 'Wakil Dekan 2 FMIPA'),
(65, 'Wakil Dekan 3 FMIPA'),
(66, 'Dekan FASILKOM'),
(67, 'Wakil Dekan 1 Fasilkom'),
(68, 'Wakil Dekan 2 Fasilkom'),
(69, 'Wakil Dekan 3 Fasilkom'),
(70, 'Dekan FKM'),
(71, 'Wakil Dekan 1 FKM'),
(72, 'Wakil Dekan 2 FKM'),
(73, 'Wakil Dekan 3 FKM'),
(74, 'Direktur Pasca Sarjana'),
(75, 'Asisten Direktur I Pasca Sarjana'),
(76, 'Asisten Direktur II Pasca Sarjana'),
(77, 'Ketua Unit Fakultas'),
(78, 'Ketua Laboratorium'),
(79, 'Ketua Prodi Akuntansi D-III'),
(80, 'Ketua Prodi Kesekretariatan D-III'),
(81, 'Ketua Prodi Manajeman InformatikA D-III'),
(82, 'Ketua Prodi Komputerisasi Akuntansi D-III'),
(83, 'Ketua Prodi Teknik Komputer D-III'),
(84, 'Ketua Prodi Manajemen S1'),
(85, 'Ketua Prodi Ekonomi  Pembangunan S1'),
(86, 'Ketua Prodi Akuntansi S1'),
(87, 'Ketua Prodi Ilmu Hukum S1'),
(88, 'Ketua Prodi Teknik  Sipil S1'),
(89, 'Ketua Prodi Teknik  Pertambangan S1'),
(90, 'Ketua Prodi Teknik  Kimia S1'),
(91, 'Ketua Prodi Teknik  Elektro S1'),
(92, 'Ketua Prodi Teknik  Mesin S1'),
(93, 'Ketua Prodi Arsitektur S1'),
(94, 'Ketua Prodi Pendidikan Dokter S1'),
(95, 'Ketua Prodi Ilmu Keperawatan S1'),
(96, 'Ketua Prodi Pendidikan Dokter Gigi S1'),
(97, 'Ketua Prodi Psikologi S1'),
(98, 'Ketua Prodi Agribisnis S1'),
(99, 'Ketua Prodi Teknik Pertanian S1'),
(100, 'Ketua Prodi Teknologi Hasil Pertanian S1'),
(101, 'Ketua Prodi Peternakan S1'),
(102, 'Ketua Prodi Budidaya Perairan S1'),
(103, 'Ketua Prodi Teknologi Hasil Perikanan S1'),
(104, 'Ketua Prodi Agroekoteknologi S1'),
(105, 'Ketua Prodi Pendidikan Bahasa Inggris S1'),
(106, 'Ketua Prodi Pendidikan Bahasa Indonesia S1'),
(107, 'Ketua Prodi Pendidikan Ekonomi S1'),
(108, 'Ketua Prodi Pendidikan Sejarah S1'),
(109, 'Ketua Prodi Pendidikan Pancasila dan Kewarganegaraan S1'),
(110, 'Ketua Prodi Pendidikan Jasmani & Kesehatan S1'),
(111, 'Ketua Prodi Bimbingan dan  konseling S1'),
(112, 'Ketua Prodi Pendidikan Matematika S1'),
(113, 'Ketua Prodi Pendidikan Biologi S1'),
(114, 'Ketua Prodi Pendidikan Kimia S1'),
(115, 'Ketua Prodi Pendidikan Fisika S1'),
(116, 'Ketua Prodi Pendidikan Teknik Mesin S1'),
(117, 'Ketua Prodi PGSD S1'),
(118, 'Ketua Prodi PG-PAUD S1'),
(119, 'Ketua Prodi Ilmu Administrasi Negara S1'),
(120, 'Ketua Prodi Sosiologi S1'),
(121, 'Ketua Prodi Matematika S1'),
(122, 'Ketua Prodi Fisika S1'),
(123, 'Ketua Prodi Kimia S1'),
(124, 'Ketua Prodi Biologi S1'),
(125, 'Ketua Prodi Ilmu kelautan S1'),
(126, 'Ketua Prodi Farmasi S1'),
(127, 'Ketua Prodi Sistem Komputer S1'),
(128, 'Ketua Prodi Teknik  Informatika S1'),
(129, 'ketua Prodi Sistem Informasi S1'),
(130, 'Ketua Prodi Ilmu Kesehatan Masyarakat S1'),
(131, 'Ketua Prodi Ilmu Tanah S1'),
(132, 'Ketua Prodi Agronomi S1'),
(133, 'Ketua Prodi Hama dan Penyakit Tumbuhan S1'),
(134, 'Ketua Prodi Pend. Profesi Akuntansi'),
(135, 'Ketua Prodi Profesi Dokter '),
(136, 'Ketua Prodi Keperawatan (NERS)'),
(137, 'Ketua Prodi Profesi Dokter Gigi '),
(138, 'Ketua Prodi Manajemen S2'),
(139, 'Ketua Prodi Ilmu Tanaman S2'),
(140, 'Ketua Prodi Agribisnis S2'),
(141, 'Ketua Prodi Ilmu Ekonomi S2'),
(142, 'Ketua Prodi Ilmu Hukum S2'),
(143, 'Ketua Prodi Pendidikan Bahasa S2'),
(144, 'Ketua Prodi Teknik Kimia S2'),
(145, 'Ketua Prodi Biomedik S2'),
(146, 'Ketua Prodi Teknik Sipil S2'),
(147, 'Ketua Prodi Pengelolaan Lingkungan S2'),
(148, 'Ketua Prodi Administrasi Publik S2'),
(149, 'Ketua Prodi Pendidikan Matematika S2'),
(150, 'Ketua Prodi Teknologi Pendidikan S2'),
(151, 'Ketua Prodi Kenotariatan S2'),
(152, 'Ketua Prodi Kependudukan S2'),
(153, 'Ketua Prodi Ilmu Manajemen S2'),
(154, 'Ketua Prodi Sosiologi S2'),
(155, 'Ketua Prodi Teknik Mesin S2'),
(156, 'Ketua Prodi Teknik Pertambangan S2'),
(157, 'Ketua Prodi Ilmu Pertanian S3'),
(158, 'Ketua Prodi Ilmu Lingkungan S3'),
(159, 'Ketua Prodi Ilmu Hukum S3'),
(160, 'Ketua Prodi Ilmu Ekonomi S3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kategori`
--

CREATE TABLE IF NOT EXISTS `master_kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data untuk tabel `master_kategori`
--

INSERT INTO `master_kategori` (`id_kategori`, `nama_kategori`) VALUES
(3, 'Pendidikan'),
(4, 'Penelitian'),
(5, 'Pengabdian Masyarakat'),
(6, 'Unsur Penunjang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_kpi`
--

CREATE TABLE IF NOT EXISTS `master_kpi` (
  `id_kpi` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi_kpi` varchar(255) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  PRIMARY KEY (`id_kpi`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=169 ;

--
-- Dumping data untuk tabel `master_kpi`
--

INSERT INTO `master_kpi` (`id_kpi`, `deskripsi_kpi`, `satuan`, `id_kategori`) VALUES
(3, 'Daya tampung mahasiswa baru S0', 'Orang', 3),
(4, 'Daya tampung mahasiswa baru S1', 'Orang', 3),
(5, 'Daya tampung mahasiswa baru S2\r\n', 'Orang', 3),
(6, 'Jumlah mahasiswa asing yang diterima di Fakultas', 'Orang', 3),
(7, 'Persentase lulusan bersertifikat kompetensi', 'Persen', 3),
(8, 'Persentase jumlah mahasiswa  yang  diberi  afirmasi tahun …….', 'Persen', 3),
(9, 'Jumlah lulusan tepat waktu S0, lulus <=  3,5 tahun', 'Tahun', 3),
(10, 'Jumlah lulusan tepat waktu S1, lulus <= 4,5 tahun', 'Tahun', 3),
(11, 'Jumlah lulusan tepat waktu S2, lulus <=  2,5 tahun', 'Tahun', 3),
(12, 'Jumlah lulusan tepat waktu S3, lulus <=  3,5 tahun', 'Tahun', 3),
(13, 'Persentase lulusan dengan skor SULIET (%) ( BAAK) S0, lulus dengan skor SULIET > 400', 'Skor', 3),
(14, 'Persentase lulusan dengan skor SULIET (%) ( BAAK) S1, lulus dengan skor SULIET > 400', 'Skor', 3),
(15, 'Persentase lulusan dengan skor SULIET (%) ( BAAK)  S2, lulus dengan skor SULIET > 500', 'Skor', 3),
(16, 'Persentase lulusan dengan skor SULIET (%) ( BAAK) S3, lulus dengan skor SULIET > 500', 'Skor', 3),
(17, 'Jumlah Prodi yang menerapkan kurikulum berpedoman KKNI', 'Prodi', 3),
(18, 'Jumlah prodi terakreditasi unggul (A)', 'Prodi', 3),
(22, 'Rata-rata penyelesaian tugas akhir (smt)(Fakultas) S0 ( Dalam Bulan )', 'Bulan', 3),
(23, 'Rata-rata penyelesaian tugas akhir (smt)(Fakultas) S1 ( Dalam Bulan )', 'Bulan', 3),
(24, 'Rata-rata penyelesaian tugas akhir (smt)(Fakultas)  S2 ( Dalam Bulan )', 'Bulan', 3),
(25, 'Rata-rata penyelesaian tugas akhir (smt)(Fakultas) S3 ( Dalam Bulan )', 'Bulan', 3),
(26, 'Jumlah Program Studi / fakultas yg ada (BAPSI) S0', 'Prodi', 3),
(27, 'Jumlah Program Studi / fakultas yg ada (BAPSI) S1', 'Prodi', 3),
(28, 'Jumlah Program Studi / fakultas yg ada (BAPSI) S2', 'Prodi', 3),
(29, 'Jumlah Program Studi / fakultas yg ada (BAPSI)  S3', 'Prodi', 3),
(30, 'Jumlah Program Studi / fakultas yg ada (BAPSI)  Fakultas', 'Fakultas', 3),
(31, 'Jumlah penelitian yang didanai tingkat nasional', 'Penelitian', 4),
(32, 'Jumlah penelitian yang didanai tingkat internasional', 'Penelitian', 4),
(33, 'Jumlah penelitian unggulan profesi', 'Penelitian', 4),
(34, 'Jumlah kontrak kerjasama penelitian dengan lembaga nasional', 'Kontrak', 4),
(35, 'Jumlah dosen mengikuti pelatihan penulisan publikasi penelitian', 'Orang', 4),
(36, 'Jumlah dosen yang mempresentasikan hasil penelitian pada seminar nasional', 'Orang', 4),
(37, 'Jumlah dosen yang mempresentasikan hasil penelitian pada seminar internasional', 'Orang', 4),
(38, 'Jumlah publikasi hasil penelitian di jurnal nasional terakreditasi', 'Jurnal', 4),
(39, 'Jumlah publikasi hasil penelitian di jurnal internasional bereputasi Scopus', 'Jurnal', 4),
(40, 'Jumlah publikasi hasil penelitian di jurnal internasional bereputasi Thompson-Reuters', 'Jurnal', 4),
(41, 'Jumlah publikasi hasil penelitian di jurnal internasional bereputasi DOAJ', 'Jurnal', 4),
(42, 'Jumlah karya tulis dosen yang dijadikan referensi/acuan oleh penulis/peneliti lain', 'Karya Tulis', 4),
(43, 'Jumlah seminar nasional yang diadakan Unsri', 'Seminar', 4),
(44, 'Jumlah seminar internasional yang diadakan Unsri', 'Seminar', 4),
(45, 'Jumlah jurnal di Fakultas/Program yang terakreditasi', 'Jurnal', 4),
(46, 'Jumlah HKI (kumulatif)', 'HKI', 4),
(47, 'Jumlah usulan/didaftarkan HKI ke Ditjen HKI  per tahun', 'Usulan', 4),
(48, 'Jumlah dosen mengikuti pelatihan penulisan proposal PPM ', 'Orang', 5),
(49, 'Jumlah dosen yang terlibat pengabdian kepada masyarakat', 'Orang', 5),
(50, 'Jumlah Kegiatan pengabdian kepada masyarakat', 'Kegiatan', 5),
(51, 'Jumlah kegiatan pengabdian kepada masyarakat yang didanai dari dalam PT', 'Kegiatan', 5),
(52, 'Jumlah kegiatan pengabdian kepada masyarakat yang didanai dari luar PT (dalam negeri)', 'Kegiatan', 5),
(53, 'Jumlah kegiatan pengabdian kepada masyarakat yang didanai dari luar negeri', 'Kegiatan', 5),
(54, 'Jumlah kegiatan pengabdian masyarakat berbasis hasil penelitian dosen yang bersangkutan', 'Kegiatan', 5),
(55, 'Jumlah publikasi hasil pengabdian kepada masyarakat skala nasional', 'Jurnal', 5),
(56, 'Jumlah publikasi hasil pengabdian kepada masyarakat skala internasional', 'Jurnal', 5),
(57, 'Jumlah dosen sebagai tenaga ahli/nara sumber dalam aplikasi IPTEKS di masyarakat', 'Orang', 5),
(58, 'Unsri masuk top 2200 dunia (versi Scopus)', 'Jurnal', 3),
(59, 'Unsri berakreditasi A (unggul)', 'Jurnal', 3),
(60, 'Terwujudnya taman sains dan teknologi  berupa Pusat unggulan', 'Tempat', 3),
(61, 'Terwujudnya taman sains dan teknologi  berupa Pusat inovasi dan kreasi', 'Tempat', 3),
(62, 'Layanan Perpustakaan Jumlah kunjungan per tahun', 'Tahun', 3),
(63, 'Layanan Perpustakaan Jumlah pengakses e-library (kunjungan)', 'Orang', 3),
(64, 'Jumlah judul jurnal nasional yang dilanggan', 'Jurnal', 3),
(65, 'Jumlah judul jurnal internasional yang dilanggan', 'Jurnal', 3),
(66, 'Jumlah judul buku teks berumur < 5 tahun', 'Buku', 3),
(67, 'Layanan Laboratorium(Sumber Data Fakultas, LDB dan PPLH) : Jumlah lab terakreditasi KAN', 'Laboratorium', 3),
(68, 'Layanan Laboratorium(Sumber Data Fakultas, LDB dan PPLH) : Rasio laboran : lab', 'Rasio', 3),
(69, 'Jumlah Laboran lab bersertifikat ', 'Jumlah', 3),
(70, 'Layanan Laboratorium(Sumber Data Fakultas, LDB dan PPLH) : Ketersediaan ruang lab (m2/mhs praktikum)', 'Laboratorium', 3),
(71, 'Jumlah sarpras dan fasilitas TI yang direvitalisasi', 'Buah', 3),
(72, 'Jumlah Kualifikasi dosen (%)(Kepegawaian KPA Unsri) Bergelar S3', 'Orang', 3),
(73, 'Jumlah Kualifikasi dosen (%)(Kepegawaian KPA Unsri) Bergelar S2', 'Orang', 3),
(74, 'Jumlah Kualifikasi dosen (%)(Kepegawaian KPA Unsri) Berjabatan Guru Besar', 'Orang', 3),
(75, 'Jumlah Kualifikasi dosen (%)(Kepegawaian KPA Unsri)  Berjabatan Lektor Kepala', 'Orang', 3),
(76, 'Jumlah Kualifikasi dosen (%)(Kepegawaian KPA Unsri)  Bersertifikat pendidik', 'Orang', 3),
(77, 'Jumlah mahasiswa penerima beasiswa (orang) (BAAK) S0', 'Orang', 3),
(78, 'Jumlah mahasiswa penerima beasiswa (orang) (BAAK) S1', 'Orang', 3),
(79, 'Jumlah mahasiswa penerima beasiswa (orang) (BAAK) S2', 'Orang', 3),
(80, 'Jumlah mahasiswa penerima beasiswa (orang) (BAAK) S3', 'Orang', 3),
(81, 'Jumlah mahasiswa penerima beasiswa (orang) (BAAK) PPDS', 'Orang', 3),
(82, 'Persentase lulusan mendapat pekerjaan pertama <3 bulan setelah lulus (%)(CDC) S0', 'Persen', 3),
(83, 'Persentase personalia dalam bidang keuangan, PPK, pengadaan barang/jasa yang telah mendapatkan pelatihan SPIP, penyusunan anggaran', 'Persen', 6),
(84, 'Persentase usulan program/kegiatan yang sesuai dgn renstra/renop', 'Orang', 6),
(85, 'Opini auditor dari auditor eksternal (BPK atau KAP)', 'Persen', 6),
(86, 'Jumlah temuan Itjen Kemristekdikti yang ditindaklanjuti', 'Buah', 6),
(87, 'Jumlah temuan oleh SPI yang ditindaklanjuti', 'Buah', 6),
(88, 'Persentase kesesuaian latar belakang pendidikan formal dan informal pegawai bagian keuangan dan bagian akuntansi dengan tupoksinya', 'Persen', 6),
(89, 'Perbedaan tanggal Laporan Keuangan dengan tanggal penyelesaian Laporan Keuangan', 'Hari', 6),
(90, 'Tingkat Ketaatan terhadap mekanisme penerimaan anggaran dan SOP Penerimaan', 'Persen', 6),
(91, 'Persentase jumlah bendaharawan penerimaan dan bendahawan pembantu yang berlatar belakang pendidikan bendaharawan', 'Persen', 6),
(92, 'Tingkat kesesuaian  waktu (jumlah hari/jam realisasi)  layanan proses pengusulan dan pencairan dana (pengeluaran uang) untuk semua jenis pengeluaran dengan standar waktu(hari/jam) per proses  yang telah ditetapkan dalam SOP', 'Hari/Jam', 6),
(93, 'Rasio perbandingan antara realisasi output kegiatan dengan target output kegiatan', 'Persen', 6),
(94, 'Rasio varian (selisih)  lebih/kurang antara realisasi dan anggaran', 'Persen', 6),
(95, 'Tingkat ketepatan waktu realisasi anggaran dengan kalender(jadwal) program universitas, fakultas, dan unit lainnya yang telah disahkan sebelumnya', 'Persen', 6),
(96, 'Indeks kepuasan pelayanan stakeholder', 'Persen', 6),
(98, 'Persentase biaya operasional terhadap pendapatan ', 'Persen', 6),
(99, 'Jumlah temuan  hasil audit atas pengadaan barang dan jasa yang ditindaklanjuti', 'Buah', 6),
(100, 'Persentase jumlah proses pengadaan yang tidak tepat waktu', 'Persen', 6),
(101, 'Persentase pengadaan barang yang dilaksanakan secara elektronik (e-procurement)', 'Persen', 6),
(102, 'Persentase jumlah sanggahan yang ditindaklanjuti', 'Persen', 6),
(103, 'Persentase tenaga kependidikan yang berkualifikasi S1', 'Persen', 6),
(104, 'Persentase tenaga kependidikan yang berkualifikasi S2', 'Persen', 6),
(105, 'Persentase tenaga kependidikan yang berkualifikasi S3', 'Persen', 6),
(106, 'Persentase tenaga kependidikan yang latar belakang pendidikannya sesuai pekerjaannya', 'Persen', 6),
(107, 'Persentase  tenaga laboran dengan laboratorium', 'Persen', 6),
(108, 'Persentase laboran/teknisi mendapat pelatihan kompetensi yang sesuai dengan bidangnya', 'Persen', 6),
(109, 'Persentase laboran/teknisi yang mendapat pelatihan sertifikasi bidangnya', 'Persen', 6),
(110, 'Persentase tenaga kependidikan Mendapat pelatihan sesuai dengan pekerjaannya', 'Persen', 6),
(111, 'Jumlah tenaga kependidikan bersertifikat pengadaan barang dan jasa', 'Orang', 6),
(113, 'Persentase  ketersediaan SOP layanan semua jenis/macam/bentuk surat menyurat (contoh; surat izin belajar, surat tugas, dll)', 'Persen', 6),
(114, 'Rata-rata tingkat ketepatan realisasi waktu proses pengurusan dan penyelesaian surat menyurat sesuai standar waktu yang ditetapkan dalam SOP (hasil sensus/observasi/ audit atas dokumen/agenda otorisasi)', 'Persen', 6),
(115, 'Rata-rata tingkat ketepatan realisasi waktu setiap tahap  proses pengurusan kepangkatan sesuai standar waktu yang ditetapkan dalam SOP ', 'Persen', 6),
(116, 'Sebelum Lektor Kepala (jumlah hari)(Kepegawaian dan Pusbangdik) Validasi', 'Hari', 6),
(117, 'Penilaian Angka Kredit Sebelum Lektor Kepala (jumlah hari)(Kepegawaian dan Pusbangdik) Bidang I dan IV', 'Skor', 6),
(118, 'Penilaian Angka Kredit Sebelum Lektor Kepala (jumlah hari)(Kepegawaian dan Pusbangdik) Bidang II dan III', 'Skor', 6),
(119, 'Lektor Kepala dan Guru Besar(Kepegawaian dan Pusbangdik)Validasi', 'Skor', 6),
(120, 'Penilaian Angka Kredit Lektor Kepala dan Guru Besar (Kepegawaian dan Pusbangdik) Bidang I dan IV', 'Skor', 6),
(121, 'Penilaian Angka Kredit Lektor Kepala dan Guru Besar (Kepegawaian dan Pusbangdik) Bidang II dan III', 'Skor', 6),
(122, 'Jumlah mahasiswa yang berwirausaha Penilaian Angka Kredit Lektor Kepala dan Guru Besar (Kepegawaian dan Pusbangdik)', 'Mahasiswa', 6),
(123, 'Jumlah mahasiswa peraih emas tingkat internasional', 'Orang', 6),
(124, 'Rasio Pendapatan PNBP', 'Persen', 6),
(125, 'Jumlah mahasiswa yang terlibat pengadian kepada masyarakat', 'Orang', 5),
(126, 'Jumlah kegiatan dan mahasiswa pada KKN', 'Buah', 5),
(127, 'Jumlah kegiatan dan mahasiswa pada KKN tematik', 'Buah', 5),
(128, 'Jumlah kegiatan dan mahasiswa pada KKN mandiri', 'Buah', 5),
(129, 'Jumlah kegiatan dan mahasiswa pada KKN bersama BKS PTN', 'Buah', 5),
(130, 'Jumlah kelompok usaha bisnis/industri binaan Unsri', 'Buah', 5),
(131, 'Jumlah binaan UKM', 'Buah', 6),
(132, 'Jumlah PKM berbasis penelitian', 'Buah', 4),
(134, 'Jumlah Pendapatan BLU', 'Pendapatan', 6),
(135, 'Jumlah kegiatan bhakti sosial', 'Buah', 5),
(137, 'Jumlah kegiatan kemahasiswaan yang bersifat Nasional', 'Buah', 6),
(138, 'Jumlah kegiatan kemahasiswaan yang bersifat Internasional', 'Buah', 6),
(139, 'Jumlah mahasiswa peraih emas tingkat nasional', 'Orang', 6),
(141, 'Jumlah dan sumber anggaran beasiswa (Juta Rupiah) (BAAK) Pemerintah ', 'Jumlah', 3),
(142, 'Jumlah dan sumber anggaran beasiswa (Juta Rupiah) (BAAK) Swasta dan pihak ketiga lainnya (PR4)', 'Jumlah', 3),
(143, 'Jumlah dan sumber anggaran beasiswa (Juta Rupiah) (BAAK) Alumni (jalur kemahasiswaan)', 'Jumlah', 3),
(144, 'Jumlah dan sumber anggaran beasiswa (Juta Rupiah) (BAAK) Internal', 'Jumlah', 3),
(145, 'Daya tampung mahasiswa baru S3', 'Orang', 3),
(146, 'Persentase lulusan mendapat pekerjaan pertama <3 bulan setelah lulus (%)(CDC) S1', 'Persen', 3),
(147, 'Persentase lulusan mendapat pekerjaan pertama <3 bulan setelah lulus (%)(CDC) S2', 'Persen', 3),
(148, 'Jumlah mahasiswa penerima beasiswa berdasarkan sumber beasiswa Pemerintah (Kemristekdikti)\r\n', 'Jumlah', 3),
(149, 'Jumlah mahasiswa penerima beasiswa berdasarkan sumber beasiswa Pemerintah (selain Kemristekdikti) ', 'Jumlah', 3),
(150, 'Jumlah mahasiswa penerima beasiswa berdasarkan sumber beasiswa: Swasta dan pihak ketiga lainnya\r\n', 'Jumlah', 3),
(151, 'Jumlah mahasiswa penerima beasiswa berdasarkan sumber beasiswa: Alumni\r\n', 'Jumlah', 3),
(152, 'Jumlah mahasiswa penerima beasiswa berdasarkan sumber beasiswa Internal\r\n', 'Jumlah', 3),
(153, 'Jumlah kerjasama kegiatan pengabdian kepada masyarakat dengan stakeholder eksternal dan alumni', 'Jumlah', 5),
(154, 'Jumlah desa binan Unsri', 'Jumlah', 5),
(155, 'Jumlah (%) pendapatan yang diperoleh dari kegiatan pengabdian kepada masyarakat kerjasama terhadap total PNBP\r\n', 'Persen', 5),
(156, 'Jumlah mahasiswa S1 yang terlibat dalam penelitian dosen (LEMLIT dan Fakultas)', 'Mahasiswa', 4),
(157, 'Jumlah mahasiswa S2 yang terlibat dalam penelitian dosen (LEMLIT dan Fakultas)', 'Mahasiswa', 4),
(158, 'Jumlah mahasiswa S3 yang terlibat dalam penelitian dosen (LEMLIT dan Fakultas)', 'Mahasiswa', 4),
(159, 'Skor kepuasan pelanggan (termasuk mahasiswa) terhadap layanan lab penelitian', 'Skor', 4),
(160, 'Produk hasil penelitian yang diaplikasikan ke masyarakat dan industri', 'Produk', 4),
(161, 'Jumlah pendapatan (%) yang diperoleh dari kegiatan penelitian kerjasama terhadap total PNBP', 'Persen', 4),
(162, 'Jumlah HaKI yang dimanfaatkan masyarakat (pihak eksternal) per tahun', 'HAKI', 4),
(163, 'Jumlah aplikasi iptek kolaborasi internasional', 'Aplikasi', 4),
(164, 'Persentase Biaya operasional yang dibiayai oleh PNBP', 'Persen', 6),
(165, 'Persentase ketepatan jadwal dalam pentahapan penyusunan rencana anggaran', 'Persen', 6),
(166, 'Bandwidth (Kbps/mhs) Sistem Informasi ( ICT Unsri)', 'Kbps/mhs', 6),
(167, 'Sistem Informasi ( ICT Unsri) Cakupan jaringan Internet dan Intranet pada  bangunan dan lingkungan akademik (%)', 'Persen', 6),
(168, 'Sistem Informasi ( ICT Unsri) Layanan manajemen berbasis TI (tambahkan indikator layanan apa saja)', 'Layanan', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_capaian`
--

CREATE TABLE IF NOT EXISTS `table_capaian` (
  `id_capaian` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(11) NOT NULL,
  `total_capaian` double NOT NULL,
  `bulan` date DEFAULT NULL,
  PRIMARY KEY (`id_capaian`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `table_capaian`
--

INSERT INTO `table_capaian` (`id_capaian`, `id_jabatan`, `total_capaian`, `bulan`) VALUES
(1, 12, 678, '2016-12-16'),
(2, 11, 1515, '2016-12-16'),
(3, 13, 310, '2016-12-16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `unit_indikator`
--

CREATE TABLE IF NOT EXISTS `unit_indikator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indikator` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `admin_mengisi`
--
ALTER TABLE `admin_mengisi`
  ADD CONSTRAINT `admin_mengisi_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `master_admin` (`id`),
  ADD CONSTRAINT `admin_mengisi_ibfk_2` FOREIGN KEY (`id_jabatan_dosen`) REFERENCES `master_jabatan` (`id_jabatan`);

--
-- Ketidakleluasaan untuk tabel `bobot_kategori`
--
ALTER TABLE `bobot_kategori`
  ADD CONSTRAINT `bobot_kategori_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `master_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bobot_kategori_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `master_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `dosen_dtt`
--
ALTER TABLE `dosen_dtt`
  ADD CONSTRAINT `dosen_dtt_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `master_dosen` (`id`),
  ADD CONSTRAINT `dosen_dtt_ibfk_2` FOREIGN KEY (`id_dosen`) REFERENCES `master_dosen` (`id`),
  ADD CONSTRAINT `dosen_dtt_ibfk_3` FOREIGN KEY (`id_jabatan`) REFERENCES `master_jabatan` (`id_jabatan`);

--
-- Ketidakleluasaan untuk tabel `kpi_dtt_tahunan`
--
ALTER TABLE `kpi_dtt_tahunan`
  ADD CONSTRAINT `kpi_dtt_tahunan_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `master_jabatan` (`id_jabatan`),
  ADD CONSTRAINT `kpi_dtt_tahunan_ibfk_2` FOREIGN KEY (`id_kpi`) REFERENCES `master_kpi` (`id_kpi`);

--
-- Ketidakleluasaan untuk tabel `master_kpi`
--
ALTER TABLE `master_kpi`
  ADD CONSTRAINT `master_kpi_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `master_kategori` (`id_kategori`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
