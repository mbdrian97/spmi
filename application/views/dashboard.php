<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- Data dosen -->
            <div class="col-lg-3">
                <a href="<?php echo site_url('dosen'); ?>">
                    <div class="widget style1 navy-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-user fa-4x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span class="font-bold">Data</span>
                                <h2 class="font-bold">Dosen</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Data jabatan -->
            <div class="col-lg-3">
                <a href="<?php echo site_url('jabatan'); ?>">
                    <div class="widget style1 blue-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-list-alt fa-4x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span class="font-bold">Data</span>
                                <h2 class="font-bold">Jabatan</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Data PKI -->
            <div class="col-lg-3">
                <a href="<?php echo site_url('kpi'); ?>">
                    <div class="widget style1 lazur-bg">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-archive fa-4x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <span class="font-bold">Key Performace Index</span>
                                <h2 class="font-bold">(KPI)</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Data KPI UNSRI -->
            <div class="col-lg-3">
                <a href="<?php echo site_url('kpi_unsri'); ?>">
                    <div class="widget style1 red-bg">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-archive fa-4x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <span class="font-bold">Key Performace Index</span>
                                <h2 class="font-bold">KPI-UNSRI</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Data Adminitrator -->
            <div class="col-lg-3">
                <a href="<?php echo site_url('admin') ?>">
                    <div class="widget style1 yellow-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-users fa-4x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span class="font-bold h5">Administrator</span>
                                <h2 class="font-bold">Users</h2>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('PKI-Rektor Admin', 'Welcome to <?php echo $_SESSION["name"]; ?>');

        }, 1300);
    });
</script>
