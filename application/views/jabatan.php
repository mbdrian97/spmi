<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><strong>Data</strong> <?php echo $page; ?></h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-center p-md">

                    <h2><span class="text-navy">INSPINIA - Responsive Admin Theme</span>
                    is provided with two main layouts <br/>three skins and separate configure options.</h2>

                    <p>
                        All config options you can trun on/off from the theme box configuration (green icon on the left side of page).
                    </p>


                </div>
            </div>
        </div>
    </div> -->
    <!-- Jabatan -->
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Jabatan Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Deskripsi Jabatan</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1;
                    foreach ($jabatan as $row) {?>
                        <tr class="gradeX">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $row['deskripsi_jabatan'] ?></td>
                            <td><a data-toggle="modal" href="#modal-update1-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                                <div id="modal-update1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                    <p>Masukkan data deskripsi jabatan.</p>
                                                    <form role="form" method="post" action="<?php echo site_url('jabatan/update'); ?>">
                                                        <input name='id' value="<?php echo $row['id_jabatan']; ?>" type="hidden" required="">
                                                        <div class="form-group">
                                                            <label>Nama</label>
                                                            <input name='deskripsi_jabatan' value="<?php echo $row['deskripsi_jabatan']; ?>" type="text" placeholder="Deskripsi" class="form-control" required="">
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-sm btn-danger pull-right m-t-n-xs" type="submit"><strong>Update</strong></button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <a data-toggle="modal" href="#modal-delete1-<?php echo $i; ?>"><i class="fa fa-trash"></i> Delete</a>
                                <div id="modal-delete1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-12"><h3 class="m-t-none m-b">Are You Sure?</h3>

                                                        <p>Apakah anda yakin ingin mengapus data jabatan: <strong><?php echo $row['deskripsi_jabatan']; ?></strong>.</p>

                                                        <form role="form" method="post" action="<?php echo site_url('jabatan/delete'); ?>">
                                                            <div class="col-sm-12"><input name='id' value="<?php echo $row['id_jabatan']; ?>" type="hidden" required=""></div>
                                                            <div>
                                                                <button class="btn btn-sm btn-danger pull-right" type="submit"><strong>Delete</strong></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php $i++;} ?>
                    </tfoot>
                    </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Form Jabatan <small>Form untuk menambahkan daftar jabatan</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="post" action="<?php echo site_url('jabatan/submit'); ?>">
                                    <div class="form-group">
                                        <label>Deskripsi Jabatan</label>
                                        <input name='deskripsi_jabatan' type="text" placeholder="Deskripsi" class="form-control" required="">
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- Relasi Jabatan -->
    <div class="row" id='relasi'>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Hubungan Atasan-Bawahan Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables2" >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Atasan</th>
                                    <th>Bawahan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;
                                foreach ($relasi_jabatan as $row) {?>
                                    <tr class="gradeX">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['atasan']; ?></td>
                                        <td><?php echo $row['bawahan']; ?></td>
                                        <td><a data-toggle="modal" href="#modal-update2-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                                            <div id="modal-update2-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <p>Masukkan data deskripsi jabatan.</p>
                                                                    <form role="form" method="post" action="<?php echo site_url('jabatan/update_relasi'); ?>">
                                                                        <div class="form-group"><input name='id' value="<?php echo $row['id_atasan_bawahan']; ?>" type="hidden" required=""></div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Atasan</label>
                                                                            <div class="col-sm-10">
                                                                                <select class="form-control m-b" name="atasan" required="">
                                                                                    <?php foreach ($jabatan as $list) {
                                                                                        if($list['deskripsi_jabatan']==$row['atasan']) $selected='selected';
                                                                                        else $selected=""; ?>
                                                                                        <option value="<?php echo $list['id_jabatan']; ?>" <?php echo $selected; ?>><?php echo $list['deskripsi_jabatan']; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Bawahan</label>
                                                                            <div class="col-sm-10">
                                                                                <select class="form-control m-b" name="bawahan" required="">
                                                                                    <?php foreach ($jabatan as $list) {
                                                                                        if($list['deskripsi_jabatan']==$row['bawahan']) $selected='selected';
                                                                                        else $selected=""; ?>
                                                                                        <option value="<?php echo $list['id_jabatan']; ?>" <?php echo $selected; ?>><?php echo $list['deskripsi_jabatan']; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a data-toggle="modal" href="#modal-delete2-<?php echo $i; ?>"><i class="fa fa-trash"></i> Delete</a>
                                            <div id="modal-delete2-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12"><h3 class="m-t-none m-b">Are You Sure?</h3>
                                                                    <p>Apakah anda yakin ingin mengapus data relasi jabatan: <br><strong><?php echo $row['atasan']; ?></strong> - <strong><?php echo $row['bawahan']; ?></strong>.</p>
                                                                    <form role="form" method="post" action="<?php echo site_url('jabatan/delete_relasi'); ?>">
                                                                        <div class="col-sm-12"><input name='id' value="<?php echo $row['id_atasan_bawahan']; ?>" type="hidden" required=""></div>
                                                                        <div>
                                                                            <button class="btn btn-sm btn-danger pull-right" type="submit"><strong>Delete</strong></button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php $i++;} ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Form Atasan-Bawahan <br><small>Form untuk menghubungkan antar Atasan dan Bawahan</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="post" action="<?php echo site_url('jabatan/insert_relasi'); ?>">
                                    <div class="form-group">
                                        <label>Atasan</label>
                                        <select name="atasan" class="select2_demo_3 form-control" required="">
                                            <option></option>
                                            <?php foreach ($jabatan as $row) { ?>
                                                <option value="<?php echo $row['id_jabatan']; ?>"><?php echo $row['deskripsi_jabatan']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Bawahan</label>
                                        <select name="bawahan" class="select2_demo_3 form-control" required="">
                                            <option></option>
                                            <?php foreach ($jabatan as $row) { ?>
                                                <option value="<?php echo $row['id_jabatan']; ?>"><?php echo $row['deskripsi_jabatan']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
