<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><strong>Data</strong> <?php echo $page; ?></h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Hubungan Atasan-Bawahan Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Deskripsi Jabatan</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;
                        foreach ($jabatan as $row) {?>
                            <tr class="gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row['deskripsi_jabatan'] ?></td>
                                <td><a data-toggle="modal" href="#modal-update-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                                    <div id="modal-update-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                        <p>Masukkan data deskripsi jabatan.</p>
                                                        <form role="form" method="post" action="<?php echo site_url('jabatan/update'); ?>">
                                                            <input name='id' value="<?php echo $row['id_jabatan']; ?>" type="hidden" required="">
                                                            <div class="form-group">
                                                                <label>Nama</label>
                                                                <input name='deskripsi_jabatan' value="<?php echo $row['deskripsi_jabatan']; ?>" type="text" placeholder="Deskripsi" class="form-control" required="">
                                                            </div>
                                                            <div>
                                                                <button class="btn btn-sm btn-danger pull-right m-t-n-xs" type="submit"><strong>Update</strong></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a data-toggle="modal" href="#modal-delete-<?php echo $i; ?>"><i class="fa fa-trash"></i> Delete</a>
                                    <div id="modal-delete-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-12"><h3 class="m-t-none m-b">Are You Sure?</h3>

                                                            <p>Apakah anda yakin ingin mengapus data jabatan: <strong><?php echo $row['deskripsi_jabatan']; ?></strong>.</p>

                                                            <form role="form" method="post" action="<?php echo site_url('jabatan/delete'); ?>">
                                                                <div class="col-sm-12"><input name='id' value="<?php echo $row['id_jabatan']; ?>" type="hidden" required=""></div>
                                                                <div>
                                                                    <button class="btn btn-sm btn-danger pull-right" type="submit"><strong>Delete</strong></button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php $i++;} ?>
                        </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Form Jabatan <small>Form untuk menambahkan daftar jabatan</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="post" action="<?php echo site_url('jabatan/submit'); ?>">
                                    <div class="form-group">
                                        <label>Deskripsi Jabatan</label>
                                        <input name='deskripsi_jabatan' type="text" placeholder="Deskripsi" class="form-control" required="">
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>