<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><strong>Data</strong> <?php echo $page; ?></h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- KPI -->
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data KPI Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables2" >
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;
                foreach ($admin as $row) {?>
                    <tr class="gradeX">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['username']; ?></td>
                        <td><?php echo $row['password']; ?></td>
                        <td><a data-toggle="modal" href="#modal-update1-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                            <div id="modal-update1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                <p>Masukkan Data Administrator</p>
                                                <form role="form" method="post" action="<?php echo site_url('admin/update'); ?>">
                                                    <input name='id' value="<?php echo $row['id']; ?>" type="hidden" required="">
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input id="username" name="username" value="<?php echo $row['username']; ?>" placeholder="Username" class="form-control" required="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama </label>
                                                        <input id="nama_admin" name="name" value="<?php echo $row['name']; ?>" type="text" placeholder="Nama lengkap" class="form-control" required=""/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input id="password" name="password" value="<?php echo $row['password']; ?>" type="password" placeholder="Password" class="form-control" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Re-password</label>
                                                        <input id="repassword" name="repassword" type="password" placeholder="Masukkan ulang password" class="form-control" required="">
                                                    </div>
                                                    <div class="hr-line-dashed"></div>
                                                    <div class="form-group">
                                                        <label>Masukkan password anda: </label>
                                                        <input id="admin_pass" name="admin_pass" type="password" placeholder="Masukkan password anda" class="form-control" required="">
                                                    </div>
                                                    <div>
                                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <a data-toggle="modal" href="#modal-delete1-<?php echo $i; ?>"><i class="fa fa-trash"></i> Delete</a>
                            <div id="modal-delete1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12"><h3 class="m-t-none m-b">Are You Sure?</h3>

                                                    <p>Apakah anda yakin ingin mengapus data Administrator: <br><strong><?php echo $row['name']; ?></strong>.</p>

                                                    <form role="form" method="post" action="<?php echo site_url('admin/delete'); ?>">
                                                        <div class="col-sm-12"><input name='id' value="<?php echo $row['id']; ?>" type="hidden" required=""></div>
                                                        <div>
                                                            <button class="btn btn-sm btn-danger pull-right" type="submit"><strong>Delete</strong></button>
                                                        </div>
                                                    </form>
                                                </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $i++;} ?>
              </tbody>
                </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Form Administrator <small><br>Form untuk menambahkan daftar Admin</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <form role="form" method="post" action="<?php echo site_url('admin/submit'); ?>" id='form'>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input id="username" name="username" placeholder="Username" class="form-control" required="" />
                                </div>
                                <div class="form-group">
                                    <label>Nama </label>
                                    <input id="nama_admin" name="name" type="text" placeholder="Nama lengkap" class="form-control" required=""/>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input id="password" name="password" type="password" placeholder="Password" class="form-control" required="">
                                </div>
                                <div class="form-group">
                                    <label>Re-password</label>
                                    <input id="repassword" name="repassword" type="password" placeholder="Masukkan ulang password" class="form-control" required="">
                                </div>
                                <div>
                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Authorize -->
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Form Administrator <small><br>Form untuk memberikan akun hak akses</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <form role="form" method="post" action="<?php echo site_url('admin/addJabatan'); ?>" id='form'>
                                <div class="form-group">
                                    <label>Admin</label>
                                    <input id="username" name="username" placeholder="Username" class="form-control" required="" />
                                </div>
                                <div class="form-group">
                                    <label>Jabatan </label>
                                    <select class="form-control m-b" id="id_jabatan" name="id_jabatan" required="">
                                        <?php foreach($jabatan as $list){ ?>
                                        <option value="<?= $list['id_jabatan']?>"><?= $list['deskripsi_jabatan']?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div>
                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
