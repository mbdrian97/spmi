<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><strong>Data</strong> <?php echo $page; ?></h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- KPI -->
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data KPI Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kategori</th>
                    <th>Deskripsi KPI</th>
                    <th>Satuan</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;
                foreach ($kpi as $row) {?>
                    <tr class="gradeX">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['nama_kategori']; ?></td>
                        <td><?php echo $row['deskripsi_kpi']; ?></td>
                        <td><?php echo $row['satuan']; ?></td>
                        <td><a data-toggle="modal" href="#modal-update1-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                            <div id="modal-update1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                <p>Masukkan data deskripsi jabatan.</p>
                                                <form role="form" method="post" action="<?php echo site_url('kpi/update'); ?>">
                                                    <input name='id' value="<?php echo $row['id_kpi']; ?>" type="hidden" required="">
                                                    <div class="form-group">
                                                        <label>Deskripsi KPI</label>
                                                        <textarea name="deskripsi_kpi" placeholder="Deksripsi Key Performance Index" class="form-control" required=""><?php echo $row['deskripsi_kpi'] ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Satuan KPI </label>
                                                        <input name="satuan_kpi" type="text" value="<?php echo $row['satuan'] ?>" placeholder="Satuan KPI" class="form-control" required=""/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Kategori KPI</label>
                                                        <select class="form-control m-b" name="kategori_kpi" required="">
                                                            <?php foreach ($kategori as $list) {
                                                              if($list['nama_kategori']==$row['nama_kategori']) $selected='selected';
                                                              else $selected="";?>
                                                                <option class="form-control" value="<?php echo $list['id_kategori']; ?>" <?php echo $selected; ?>><?php echo $list['nama_kategori']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div>
                                                        <button class="btn btn-sm btn-danger pull-right m-t-n-xs" type="submit"><strong>Update</strong></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <a data-toggle="modal" href="#modal-delete1-<?php echo $i; ?>"><i class="fa fa-trash"></i> Delete</a>
                            <div id="modal-delete1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12"><h3 class="m-t-none m-b">Are You Sure?</h3>

                                                    <p>Apakah anda yakin ingin mengapus data KPI: <br><strong><?php echo $row['deskripsi_kpi']; ?></strong>.</p>

                                                    <form role="form" method="post" action="<?php echo site_url('kpi/delete'); ?>">
                                                        <div class="col-sm-12"><input name='id' value="<?php echo $row['id_kpi']; ?>" type="hidden" required=""></div>
                                                        <div>
                                                            <button class="btn btn-sm btn-danger pull-right" type="submit"><strong>Delete</strong></button>
                                                        </div>
                                                    </form>
                                                </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $i++;} ?>
                </tfoot>
                </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Form Key Performance Index <small><br>Form untuk menambahkan daftar KPI</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="post" action="<?php echo site_url('kpi/submit'); ?>">
                                    <div class="form-group">
                                        <label>Deskripsi KPI</label>
                                        <textarea name="deskripsi_kpi" placeholder="Deksripsi Key Performance Index" class="form-control" required=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Satuan KPI </label>
                                        <input name="satuan_kpi" type="text" placeholder="Satuan KPI" class="form-control" required=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori KPI</label>
                                        <select class="form-control m-b select2_demo_2" name="kategori_kpi" required="">
                                            <<option value=""></option>
                                            <?php foreach ($kategori as $list) { ?>
                                                <option value="<?php echo $list['id_kategori']; ?>"><?php echo $list['nama_kategori']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Pembobotan KPI<small><br>Form untuk menambahkan Bobot KPI.TENTUKAN SEMUA KPI-UNSRI PER-JABATAN DAN PER-KATEGORI TERLEBIH DAHULU</small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="post" action="<?php echo site_url('kpi/bobot'); ?>">
                                    <div class="form-group">
                                        <label>Kategori KPI</label>
                                        <select class="form-control m-b select2_demo_2" name="kategori_kpi" required="">
                                            <<option value=""></option>
                                            <?php foreach ($kategori as $list) { ?>
                                                <option value="<?php echo $list['id_kategori']; ?>"><?php echo $list['nama_kategori']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <select class="form-control m-b select2_demo_3" name="jabatan" required="">
                                            <<option value=""></option>
                                            <?php foreach ($jabatan as $list) { ?>
                                                <option value="<?php echo $list['id_jabatan']; ?>"><?php echo $list['deskripsi_jabatan']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Bobot Kategori </label>
                                        <input name="bobot" type="text" placeholder="Bobot" class="form-control" required=""/>
                                    </div>
                                    
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
