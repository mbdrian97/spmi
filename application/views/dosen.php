<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><strong>Data</strong> Dosen</h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Dosen Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                <tr>
                    <th>No</th>
                    <th>NIP</th>
                    <th>Jabatan</th>
                    <th>Nama</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;
                foreach ($data_dosen as $row) {?>
                    <tr class="gradeX">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['nip'] ?></td>
                        <td><?php echo $row['deskripsi_jabatan'] ?></td>
                        <td><?php echo $row['nama']; ?></td>
                        <td><a data-toggle="modal" href="#modal-update-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                            <div id="modal-update-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                <form role="form" method="post" action="<?php echo site_url('dosen/update'); ?>">
                                                    <input name='id' value="<?php echo $row['id']; ?>" type="hidden" required="">
                                                    <div class="form-group">
                                                        <label>NIP</label>
                                                        <input name='nip' value="<?php echo $row['nip']; ?>" type="text" data-mask="99999999 999999 9 999" placeholder="Enter NIP" class="form-control" required="">
                                                        <span class="help-block">Contoh: 99999999 999999 9 999</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input name='nama' value="<?php echo $row['nama']; ?>" type="text" placeholder="Nama Dosen" class="form-control" required="">
                                                    </div>
                                                    <div>
                                                        <button class="btn btn-sm btn-danger pull-right m-t-n-xs" type="submit"><strong>Update</strong></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <a data-toggle="modal" href="#modal-delete-<?php echo $i; ?>"><i class="fa fa-trash"></i> Delete</a>
                            <div id="modal-delete-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12"><h3 class="m-t-none m-b">Are You Sure?</h3>

                                                    <p>Apakah anda yakin ingin mengapus data dosen NIP: <strong><?php echo $row['nip']; ?></strong>.</p>

                                                    <form role="form" method="post" action="<?php echo site_url('dosen/delete'); ?>">
                                                        <div class="col-sm-12"><input name='id_dosen' value="<?php echo $row['id']; ?>" type="hidden" required=""></div>
                                                        <div>
                                                            <button class="btn btn-sm btn-danger pull-right" type="submit"><strong>Delete</strong></button>
                                                        </div>
                                                    </form>
                                                </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php $i++;} ?>
                </tfoot>
                </table>
                    </div>

                </div>
            </div>
        </div>
    
        <?php if($_SESSION['id_jabatan'] == 0){ ?>
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Form Dosen <small>Form untuk menambahkan daftar dosen</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <form role="form" method="post" action="<?php echo site_url('dosen/submit'); ?>">
                                    <div class="form-group">
                                        <label>NIP</label>
                                        <input name='nip' type="text" data-mask="99999999 999999 9 999" placeholder="Enter NIP" class="form-control" required="">
                                        <span class="help-block">Contoh: 99999999 999999 9 999</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input name='nama' type="text" placeholder="Nama Dosen" class="form-control" required="">
                                    </div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>