<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><strong>Data</strong> <?php echo $page; ?></h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Dosen DTT KPI -->
    <div class="row">
      <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data KPI Universitas Sriwijaya</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

            <div class="ibox-content">

                <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <tr>
                <th>No</th>
                <th>Jabatan</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0;
                if($this->session->userdata('id_jabatan') > 0){
                    foreach ($jabatan as $row) {
                        if($_SESSION['id_jabatan'] == $row['id_jabatan']){ ?>
                            <tr class="gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row['deskripsi_jabatan']; ?></td>
                                <td><a data-toggle="modal" href="<?php echo site_url('kpi_unsri/detail/'.$row['id_jabatan']); ?>"><i class="fa fa-edit"></i>Detail KPI</a>
                                </td>
                            </tr>
                        <?php }?>
                    <?php $i++;}
                }else{
                    foreach ($jabatan as $row) {
                        if(0 != $row['id_jabatan']){ ?>
                            <tr class="gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row['deskripsi_jabatan']; ?></td>
                                <td><a data-toggle="modal" href="<?php echo site_url('kpi_unsri/detail/'.$row['id_jabatan']); ?>"><i class="fa fa-edit"></i>Detail KPI</a>
                                </td>
                            </tr>
                        <?php }?>
                    <?php $i++;} } ?>
            </tfoot>
            </table>
                </div>
            </div>
      </div>
    </div>
    <!-- Form Dosen DTT KPI -->
    <div class="row" id='form'>
      <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Form Key Performance Index <small>Form memberikan indikator kepada setiap jabatan</small></h5>
                      <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row">
                          <div class="col-sm-12">
                              <form role="form" method="post" action="<?php echo site_url('kpi_unsri/submit'); ?>">
                                  <?php if($this->session->userdata('id_jabatan') == 0){ ?>
                                      <!-- jabatan_dtt -->
                                      <div class="form-group">
                                          <label>Jabatan</label>
                                          <select class="form-control m-b select2_demo_3" name="jabatan_dtt" required="">
                                              <option value=""></option>
                                              <?php foreach ($jabatan as $list) { 
                                                if($list['id_jabatan'] != 0){ ?>  
                                                  <option value="<?php echo $list['id_jabatan']; ?>">
                                                      <?php echo $list['deskripsi_jabatan']; ?>
                                                  </option>
                                              <?php } }?>
                                          </select>
                                      </div>
                                  <?php }else{ ?>
                                        <?php foreach ($jabatan as $list) { 
                                            if($list['id_jabatan'] == $_SESSION['id_jabatan']){ ?>  
                                              <input type="hidden" class="form-control m-b" value="<?php echo $list['id_jabatan']; ?>" name="jabatan_dtt" required="">
                                        <?php } }?>
                                  <?php }?>
                                  <!-- kpi_dtt -->
                                  <div class="form-group">
                                      <label>Key Performance Index </label>
                                      <select class="form-control m-b select2_demo_2" name="kpi_dtt" required="">
                                          <option value=""></option>
                                          <?php foreach ($kpi as $list) { ?>
                                              <option value="<?php echo $list['id_kpi']; ?>"><?php echo $list['deskripsi_kpi']; ?></option>
                                          <?php } ?>
                                      </select>
                                  </div>
                                  <div class="row">
                                    <!-- tahun berlaku kpi -->
                                    <div class="col-md-3">
                                      <div class="form-group">
                                          <label>Tahun Berlaku KPI</label>
                                          <select class="form-control m-b" name="tahun_dtt" required="">
                                            <?php $year = date("Y");
                                            if ($year%5==0) {
                                              $year--;
                                            }

                                            for ($i=1; $i <=5 ; $i++) { 
                                              $tamp = intval($year/5)*5+$i;
                                              if($tamp == date("Y")) $selected='selected';
                                              else $selected="";?>
                                                <option class="form-control" value="<?php echo $tamp; ?>" <?php echo $selected; ?>><?php echo $tamp; ?></option>
                                            <?php } ?>
                                        </select>
                                      </div>
                                    </div>
                                    <!-- baseline dtt -->
                                    <div class="col-md-3">
                                      <div class="form-group">
                                          <label>Baseline</label>
                                          <input name="baseline_dtt" placeholder="Baseline" class="form-control" required="" type="text"/>
                                      </div>
                                    </div>
                                    <!-- Capaian dtt -->
                                    <div class="col-md-3">
                                      <div class="form-group">
                                          <label>Capaian</label>
                                          <input name="capain_dtt" placeholder="Capaian" class="form-control" required="" type="text"/>
                                      </div>
                                    </div>
                                    <!-- target dtt -->
                                    <div class="col-md-3">
                                      <div class="form-group">
                                          <label>Target</label>
                                          <input name="target_dtt" placeholder="Target" class="form-control" required="" type="text"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <!-- bobot kpi -->
                                    <div class="col-md-4">
                                      <div class="form-group">
                                          <label>Bobot KPI</label>
                                          <input name="bobot_dtt" placeholder="Bobot KPI" class="form-control" required="" type="text"/>
                                      </div>
                                    </div>
                                    <!-- rasio dtt 
                                    <div class="col-md-4">
                                      <div class="form-group">
                                          <label>Rasio</label>
                                          <input name="rasio_dtt" placeholder="Rasio" class="touchspin1 form-control" required="" type="text"/>
                                          <span class="help-block">Minimun: 0 dan Maksimum: 5</span>
                                      </div>
                                    </div>-->
                                    <!-- persentase capaian dtt 
                                    <div class="col-md-4">
                                      <div class="form-group">
                                          <label>Persentase Capaian</label>
                                          <input name="persentase_capaian_dtt" placeholder="Persentase Capaian" class="touchspin2" required="" type="text"/>
                                          <span class="help-block">Nilai dalam satuan persen (%)</span>
                                      </div>
                                    </div>-->
                                  </div>
                                  <div>
                                      <button name="resubmit" class="btn btn-sm btn-danger pull-left m-t-n-xs" type="submit"><strong>Save and Input Again</strong></button>
                                      <button name='submit' class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Submit</strong></button>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
      </div>
    </div>
</div>
