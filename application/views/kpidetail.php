<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Data KPI - Jabatan: <strong><?php echo $page; ?></strong></h2>
    </div>
</div>
<!-- Body -->
<div class="wrapper wrapper-content">
    <!-- Detail Dosen DTT KPI -->
    <div class="row">
      <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <!-- <div class="ibox-title">
                <h5>KPI Universitas Sriwijaya</h5>
            </div> -->
            <div class="ibox-content">
              <form role="form" method="post" action="<?php echo site_url('kpi_unsri/detail/'.$id['id_jabatan'].'#show') ?>" class="form-horizontal">
                  <!-- id -->
                  <div class="form-group">
                    <input id='id_id' name='id' value="<?php echo $id['id_jabatan']; ?>" type="hidden" required="">
                  </div>
                  <!-- jabatan -->
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Jabatan</label>
                      <div class="col-sm-10"><input type="text" value="<?php echo $id['deskripsi_jabatan']; ?>" class="form-control" required="" disabled></div>
                  </div>
                  <!-- tahun -->
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Tahun</label>
                      <div class="col-sm-10"><!-- <input name="tahun_kpi" type="text" value="<?php echo date('Y'); ?>" class="form-control"> -->
                        <select class="form-control m-b" name="tahun_kpi" required="">
                          <?php $year = date("Y");
                          if ($year%5==0) {
                            $year--;
                          }

                          for ($i=1; $i <=5 ; $i++) { 
                            $tamp = intval($year/5)*5+$i;
                            if($tamp == date("Y")) $selected='selected';
                            else $selected="";?>
                              <option class="form-control" value="<?php echo $tamp; ?>" <?php echo $selected; ?>><?php echo $tamp; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-sm-10 col-sm-offset-2">
                          <button name="button_year_dtt" class="btn btn-success pull-right" type="submit">Tampilkan</button>
                      </div>
                  </div>
              </form>
              <!-- <div class="hr-line-dashed"></div> -->
            </div>
        </div>
      </div>
    </div>
      <div class="row animated fadeInDown" id="show">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                      <div class="col-lg-12">
                        <h3>Total Pencapaian : <strong><?php echo $total; ?></strong> - Tahun <strong><?php echo $tahun; ?></strong></h3>
                      </div>
                      <div class="hr-line-dashed"></div>
                    </div>
                    <div class="row">
                    <!-- <?php print_r($kpi_dtt_tahunan); ?> -->
                      <div class="col-lg-10">
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" >
                          <thead>
                          <tr>
                              <th style="text-align:center">No</th>
                              <th style="text-align:center">Deskripsi KPI</th>
                              <th style="text-align:center">Baseline</th>
                              <th style="text-align:center">Target</th>
                              <th style="text-align:center">Capaian</th>
                              <th style="text-align:center">Bobot</th>
                              <!-- <th style="text-align:center">Rasio</th> -->
                              <th style="text-align:center">Persentase (%)</th>
                              <th style="text-align:center">Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php $i=1;
                          $total = 0;
                          foreach ($kpi_dtt_tahunan as $row) {?>
                              <tr class="gradeX">
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $row['deskripsi_kpi'] ?></td>
                                  <td><?php echo $row['baseline'] ?></td>
                                  <td><?php echo $row['target'] ?></td>
                                  <td><?php echo $row['capaian'] ?></td>
                                  <td><?php echo $row['bobot'] ?></td>
                                  <!-- <td><?php echo $row['rasio'] ?></td> -->
                                  <td><?php echo $row['persentase_capaian'] ?></td>
                                  <td><a data-toggle="modal" href="#modal-update1-<?php echo $i; ?>"><i class="fa fa-edit"></i> Edit</a>
                                      <div id="modal-update1-<?php echo $i; ?>" class="modal fade" aria-hidden="true">
                                          <div class="modal-dialog">
                                              <div class="modal-content">
                                                  <div class="modal-body">
                                                      <div class="row">
                                                          <div class="col-sm-12">
                                                          <p>Masukkan data target.</p>
                                                          <form role="form" method="post" action="<?php echo site_url('kpi_unsri/update'); ?>">
                                                              <input name='id' value="<?php echo $row['id_kpi_dtt']; ?>" type="hidden" required="">
                                                              <div class="form-group">
                                                                  <label style="<?php if($_SESSION['id_jabatan'] != 0){ echo 'display: none'; }else{ echo 'display=block'; }?>" >Baseline</label>
                                                                  <input name='baseline' value="<?php echo $row['baseline']; ?>" <?php if($_SESSION['id_jabatan'] != 0){ echo 'type="hidden"'; }else{ echo 'type="text"'; }?> placeholder="Deskripsi" class="form-control" required="">
                                                                  <label style="<?php if($_SESSION['id_jabatan'] != 0){ echo 'display: none'; }else{ echo 'display=block'; }?>">Target</label>
                                                                  <input name='target' value="<?php echo $row['target']; ?>" <?php if($_SESSION['id_jabatan'] != 0){ echo 'type="hidden"'; }else{ echo 'type="text"'; }?> placeholder="Deskripsi" class="form-control" required="">
                                                                  <label>Capaian</label>
                                                                  <input name='capaian' value="<?php echo $row['capaian']; ?>" type="text" placeholder="Deskripsi" class="form-control" required="">
                                                                  <label style="<?php if($_SESSION['id_jabatan'] != 0){ echo 'display: none'; }else{ echo 'display=block'; }?>">Bobot</label>
                                                                  <input name='bobot' value="<?php echo $row['bobot']; ?>" <?php if($_SESSION['id_jabatan'] != 0){ echo 'type="hidden"'; }else{ echo 'type="text"'; }?> placeholder="Deskripsi" class="form-control" required="">
                                                              </div>
                                                              <div>
                                                                  <button class="btn btn-sm btn-danger pull-right m-t-n-xs" type="submit"><strong>Update</strong></button>
                                                              </div>
                                                          </form>
                                                      </div>
                                                  </div>
                                              </div>
                                              </div>
                                          </div>
                                      </div>
                                  </td>
                              </tr>
                          <?php $i++;} ?>
                          </tfoot>
                          </table>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <button class="btn" type="button" data-toggle="modal" data-target="#myModal5"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
                        <!-- start of modal -->
                        <div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title"></h4>
                                        <small class="font-bold">KPI Atasan</small>
                                    </div>
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                        <form role="form" method="post" action="<?php echo site_url('kpi_unsri/add_kpi') ?>" class="form-horizontal">
                                          <table class="table table-striped table-bordered table-hover" >
                                          <thead>
                                          <tr>
                                              <th style="text-align:center"></th>
                                              <th style="text-align:center">Deskripsi KPI</th>
                                              <th style="text-align:center">Baseline</th>
                                              <th style="text-align:center">Target</th>
                                              <th style="text-align:center">Capaian</th>
                                              <th style="text-align:center">Bobot</th>
                                              <th style="text-align:center">Rasio (1 - 5)</th>
                                              <th style="text-align:center">Persentase (%)</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <?php $i=1;
                                          foreach ($dtt_kpi as $row) {?>
                                              <tr class="gradeX">
                                                  <td>
                                                    <div class="i-checks"><input type="checkbox" name="kpi[]" value="<?php echo $row['id_kpi_dtt'] ?>"></div>
                                                  </td>
                                                  <td><?php echo $row['deskripsi_kpi'] ?></td>
                                                  <td><?php echo $row['baseline'] ?></td>
                                                  <td><?php echo $row['capaian'] ?></td>
                                                  <td><?php echo $row['target'] ?></td>
                                                  <td><?php echo $row['bobot'] ?></td>
                                                  <td><?php echo $row['rasio'] ?></td>
                                                  <td><?php echo $row['persentase_capaian'] ?></td>
                                              </tr>
                                          <?php $i++;} ?>
                                          <input id='id_id' name='jabatan' value="<?php echo $id['id_jabatan']; ?>" type="hidden" required="">
                                          </tbody>
                                          </table>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary" value="submit">Save changes</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end of modal -->
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
</div>
