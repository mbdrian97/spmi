<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url();?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url();?>assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
     <!-- FooTable -->
    <link href="<?php echo base_url();?>assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
        <!-- navigation -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url();?>assets/img/profile_small.jpg" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['name']; ?></strong>
                             </span> <span class="text-muted text-xs block">KPI-Rektor's Admin</span> </span> </a>
                        </div>
                        <div class="logo-element">
                            KPI-R
                        </div>
                    </li>
                    <li class="<?php if($page=='dashboard') echo "active";?>">
                        <a href="<?php echo site_url('home'); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                    </li>
                    <li class="<?php if($page=='dosen') echo "active";?>">
                        <a href="<?php echo site_url('dosen'); ?>"><i class="fa fa-user"></i> <span class="nav-label">Dosen</span></a>
                    </li>
                    <li class="<?php if($page=='KPI-Unsri') echo "active";?>">
                        <a href="<?php echo site_url('kpi_unsri');?>"><i class="fa fa-folder"></i> <span class="nav-label">KPI - Unsri</span></a>
                    </li>
                    
                    <?php if($this->session->userdata('id_jabatan') == 0){ ?>
                        <li class="<?php if($page=='jabatan') echo "active";?>">
                            <a href="<?php echo site_url('jabatan'); ?>"><i class="fa fa-list-alt"></i> <span class="nav-label">Jabatan</span></a>
                        </li>
                        <li class="<?php if($page=='KPI') echo "active";?>">
                            <a href="<?php echo site_url('kpi');?>"><i class="fa fa-folder-open"></i> <span class="nav-label">Key Performance Index</span></a>
                        </li>
                        <li class="<?php if($page=='Administrator') echo "active";?>">
                            <a href="<?php echo site_url('admin');?>"><i class="fa fa-stack-exchange"></i> <span class="nav-label">Administrator</span></a>
                        </li>
                    <?php }?>

                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <!-- header -->
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top <?php if($page=='dashboard') echo 'white-bg'; ?>" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <!-- <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form> -->
                    </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <a href="<?php echo site_url('auth/logout');?>">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                            <!-- <li>
                                <a class="right-sidebar-toggle">
                                    <i class="fa fa-tasks"></i>
                                </a>
                            </li> -->
                        </ul>
                </nav>
            </div>
