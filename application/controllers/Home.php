<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Check Session */
		
	}

	public function index() {
		if(!$this->session->userdata('logged_in')) {
			redirect('auth');
		}else{
			$data['title'] = 'KPI-Rektor | Dashboard';
			$data['page'] = 'dashboard';

			$this->load->view('header',$data);
			$this->load->view('dashboard');
			$this->load->view('footer');
		}
		
	}
}
