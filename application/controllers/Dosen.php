<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Check Session */
		if(!$this->session->userdata('logged_in')) redirect('auth');
		$this->load->model('model_dosen');
	}

	public function index() {
		$data['title'] = 'SPMI | Dosen';
		$data['page'] = 'dosen';
		$data['data_dosen'] = $this->model_dosen->select();

		$this->load->view('header',$data);
		$this->load->view('dosen');
		$this->load->view('footer');
	}

	public function submit() {
		$this->form_validation->set_rules('nip', 'Nip', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');

		$data = array(
			'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama')
		);

		if ($this->form_validation->run()) {
			if ($this->model_dosen->insert($data)) {
				echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
				window.location.href = '".site_url('dosen')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
				window.location.href = '".site_url('dosen')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('dosen')."';</script>";
		}
	}

	public function delete() {
		$this->form_validation->set_rules('id_dosen','Id_dosen','required');

		$id = $this->input->post('id_dosen');
		//echo $id;
		if ($this->form_validation->run()) {
			if ($this->model_dosen->delete($id)) {
				echo "<script type='text/javascript'>alert('Data berhasil dihapus');
				window.location.href = '".site_url('dosen')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal dihapus. Harap coba lagi');
				window.location.href = '".site_url('dosen')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error!');
				window.location.href = '".site_url('dosen')."';</script>";
		}
	}

	public function update() {
		$this->form_validation->set_rules('nip', 'Nip', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$id = $this->input->post('id');

		$data = array(
			'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama')
		);
		/*echo $id;
		print_r($data);*/
		if ($this->form_validation->run()) {
			if ($this->model_dosen->update($id,$data)) {
				echo "<script type='text/javascript'>alert('Data berhasil diubah');
				window.location.href = '".site_url('dosen')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal diubah. Harap coba lagi');
				window.location.href = '".site_url('dosen')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('dosen')."';</script>";
		}
	}
}
