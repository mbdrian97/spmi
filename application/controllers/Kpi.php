<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kpi extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Check Session */
		if(!$this->session->userdata('logged_in')) redirect('auth');
		$this->load->model('model_kpi');
		$this->load->model('model_kategori_kpi');
		$this->load->model('model_jabatan');
		$this->load->model('model_kpi_dtt');
		$this->load->model('model_bobot');
	}

	public function index() {
		$data['title'] = 'KPI-Rektor | Entry';
		$data['page'] = 'KPI';
		$data['kpi'] = $this->model_kpi->select();
		$data['kategori'] = $this->model_kategori_kpi->select();
		$data['jabatan'] = $this->model_jabatan->select();

		$this->load->view('header',$data);
		$this->load->view('masterkpi');
		$this->load->view('footer');
	}

	public function bobot(){
		$this->form_validation->set_rules('bobot', 'Bobot','required');
		$this->form_validation->set_rules('jabatan','Jabatan','required');
		$this->form_validation->set_rules('kategori_kpi','Kategori_kpi','required');
		$bobot_kategori = $this->input->post('bobot');
		$data = array(
			'bobot' => $this->input->post('bobot'),
			'id_jabatan' => $this->input->post('jabatan'),
			'id_kategori' => $this->input->post('kategori_kpi')
		);
		if ($this->form_validation->run()) {
			$check_bobot = $this->model_bobot->select($data['id_jabatan'],$data['id_kategori']);
			if(empty($check_bobot)){
				if ($this->model_bobot->insert($data)) {
				$jumlah = $this->model_kpi_dtt->getItemKPI($data['id_jabatan'],$data['id_kategori']);
					if($jumlah){
						$bobot_item = (double)$bobot_kategori/(double)$jumlah['jumlah'];
						if($this->model_kpi_dtt->updateBobot($bobot_item,$data['id_kategori'])){
							echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
									window.location.href = '".site_url('kpi')."';</script>";
						}
					}
				}
				else {
					echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
					window.location.href = '".site_url('kpi')."';</script>";
					}
			}else{
				if ($this->model_bobot->update($check_bobot['id'],$data)) {
				$jumlah = $this->model_kpi_dtt->getItemKPI($data['id_jabatan'],$data['id_kategori']);
					if($jumlah){
						$bobot_item = (double)$bobot_kategori/(double)$jumlah['jumlah'];
						if($this->model_kpi_dtt->updateBobot($bobot_item,$data['id_kategori'])){
							echo "<script type='text/javascript'>alert('Data berhasil diperbarui');
									window.location.href = '".site_url('kpi')."';</script>";
						}
					}
				}
				else {
						echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
					window.location.href = '".site_url('kpi')."';</script>";
					}
			}
			
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('kpi')."';</script>";
		}
	}

	public function submit() {
		$this->form_validation->set_rules('deskripsi_kpi', 'Deskripsi_kpi','required');
		$this->form_validation->set_rules('satuan_kpi','Satuan_kpi','required');
		$this->form_validation->set_rules('kategori_kpi','Kategori_kpi','required');

		$data = array(
			'deskripsi_kpi' => $this->input->post('deskripsi_kpi'),
			'satuan' => $this->input->post('satuan_kpi'),
			'id_kategori' => $this->input->post('kategori_kpi')
		);
		if ($this->form_validation->run()) {
			if ($this->model_kpi->insert($data)) {
				echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
				window.location.href = '".site_url('kpi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
				window.location.href = '".site_url('kpi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('kpi')."';</script>";
		}
	}

	public function update() {
		$this->form_validation->set_rules('id','Id','required');
		$this->form_validation->set_rules('deskripsi_kpi', 'Deskripsi_kpi','required');
		$this->form_validation->set_rules('satuan_kpi','Satuan_kpi','required');
		$this->form_validation->set_rules('kategori_kpi','Kategori_kpi','required');

		$id = $this->input->post('id');
		$data = array(
			'deskripsi_kpi' => $this->input->post('deskripsi_kpi'),
			'satuan' => $this->input->post('satuan_kpi'),
			'id_kategori' => $this->input->post('kategori_kpi')
		);

		if ($this->form_validation->run()) {
			if ($this->model_kpi->update($id,$data)) {
				echo "<script type='text/javascript'>alert('Data berhasil diperbarui');
				window.location.href = '".site_url('kpi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal diperbarui. Coba lagi');
				window.location.href = '".site_url('kpi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('kpi')."';</script>";
		}
	}

	public function delete() {
		$this->form_validation->set_rules('id','Id','required');

		$id = $this->input->post('id');
		if ($this->form_validation->run()) {
			if ($this->model_kpi->delete($id)) {
				echo "<script type='text/javascript'>alert('Data berhasil dihapus');
				window.location.href = '".site_url('kpi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal dihapus. Coba lagi');
				window.location.href = '".site_url('kpi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('kpi')."';</script>";
		}
	}

	public function submit_dtt() {
		$this->form_validation->set_rules('jabatan_dtt','Jabatan_dtt','required');
		$this->form_validation->set_rules('kpi_dtt','Kpi_dtt','required');
		$this->form_validation->set_rules('tahun_dtt','Tahun_dtt','required');
		$this->form_validation->set_rules('baseline_dtt','Baseline_dtt','required');
		$this->form_validation->set_rules('capain_dtt','Capain_dtt','required');
		$this->form_validation->set_rules('target_dtt','Target_dtt','required');
		$this->form_validation->set_rules('bobot_dtt','Bobot_dtt','required');
		$this->form_validation->set_rules('rasio_dtt','Rasio_dtt','required');
		$this->form_validation->set_rules('persentase_capaian_dtt','persentase_capaian_dtt','required');

		$data = array(
			'id_jabatan' 					=> $this->input->post('jabatan_dtt'),
			'id_kpi' 							=> $this->input->post('kpi_dtt'),
			'tahun' 							=> $this->input->post('tahun_dtt'),
			'baseline' 						=> $this->input->post('baseline_dtt'),
			'capaian'							=> $this->input->post('capain_dtt'),
			'target'							=> $this->input->post('target_dtt'),
			'bobot'								=> $this->input->post('bobot_dtt'),
			'rasio'								=> $this->input->post('rasio_dtt'),
			'persentase_capaian' 	=> $this->input->post('persentase_capaian_dtt')
		);

		if ($this->form_validation->run()) {
			if ($this->model_kpi_dtt->insert($data)) {
				echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
				window.location.href = '".site_url('kpi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
				window.location.href = '".site_url('kpi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('kpi')."';</script>";
		}
	}
}
