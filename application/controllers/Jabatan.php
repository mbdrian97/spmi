<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Check Session */
		if(!$this->session->userdata('logged_in')) redirect('auth');
		$this->load->model('model_jabatan');
		$this->load->model('model_atasan_bawahan');
	}

	public function index() {
		$data['title'] = 'KPI-Rektor | Jabatan';
		$data['page'] = 'jabatan';
		$data['jabatan'] = $this->model_jabatan->select();
		$data['relasi_jabatan'] = $this->model_atasan_bawahan->select();

		$this->load->view('header',$data);
		$this->load->view('jabatan');
		$this->load->view('footer');
	}

	public function submit() {
		$this->form_validation->set_rules('deskripsi_jabatan', 'Deskripsi_jabatan','required');

		$data = array(
			'deskripsi_jabatan' => $this->input->post('deskripsi_jabatan')
		);
		if ($this->form_validation->run()) {
			if ($this->model_jabatan->insert($data)) {
				echo "<script type='text/javascript'>alert('Data berhasil ditambahkan'); 
				window.location.href = '".site_url('jabatan')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi'); 
				window.location.href = '".site_url('jabatan')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong'); 
				window.location.href = '".site_url('jabatan')."';</script>";
		}
	}

	public function update() {
		$this->form_validation->set_rules('id','Id','required');
		$this->form_validation->set_rules('deskripsi_jabatan','Deskripsi_jabatan','required');
		$id = $this->input->post('id');

		$data = array(
			'deskripsi_jabatan' => $this->input->post('deskripsi_jabatan')
		);
		if ($this->form_validation->run()) {
			if ($this->model_jabatan->update($id,$data)) {
				echo "<script type='text/javascript'>alert('Data berhasil diperbarui'); 
				window.location.href = '".site_url('jabatan')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal diperbarui. Coba lagi'); 
				window.location.href = '".site_url('jabatan')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong'); 
				window.location.href = '".site_url('jabatan')."';</script>";
		}
	}

	public function delete() {
		$this->form_validation->set_rules('id','Id','required');

		$id = $this->input->post('id');
		if ($this->form_validation->run()) {
			if ($this->model_jabatan->delete($id)) {
				echo "<script type='text/javascript'>alert('Data berhasil dihapus'); 
				window.location.href = '".site_url('jabatan')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal dihapus. Coba lagi'); 
				window.location.href = '".site_url('jabatan')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong'); 
				window.location.href = '".site_url('jabatan')."';</script>";
		}
	}

	public function insert_relasi() {
		$this->form_validation->set_rules('atasan','Atasan','required');
		$this->form_validation->set_rules('bawahan','Bawahan','required');

		$data = array(
			'id_dtt_atasan' => $this->input->post('atasan'),
			'id_dtt_bawahan' => $this->input->post('bawahan')
		);

		if ($this->form_validation->run()) {
			if ($this->model_atasan_bawahan->insert($data)) {
				echo "<script type='text/javascript'>alert('Data relasi berhasil ditambahkan'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data relasi gagal ditambahkan. Coba lagi'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data relasi tidak boleh kosong'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
		}
	}

	public function update_relasi() {
		$this->form_validation->set_rules('id','Id','required');
		$this->form_validation->set_rules('atasan','Atasan','required');
		$this->form_validation->set_rules('bawahan','Bawahan','required');

		$id = $this->input->post('id');

		$data = array(
			'id_dtt_atasan' => $this->input->post('atasan'),
			'id_dtt_bawahan' => $this->input->post('bawahan')
		);

		if ($this->form_validation->run()) {
			if ($this->model_atasan_bawahan->update($id,$data)) {
				echo "<script type='text/javascript'>alert('Data berhasil diperbarui'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal diperbarui. Coba lagi'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
		}
	}

	public function delete_relasi() {
		$this->form_validation->set_rules('id','Id','required');

		$id = $this->input->post('id');

		if ($this->form_validation->run()) {
			if ($this->model_atasan_bawahan->delete($id)) {
				echo "<script type='text/javascript'>alert('Data berhasil dihapus'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal dihapus. Coba lagi'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong'); 
				window.location.href = '".site_url('jabatan#relasi')."';</script>";
		}
	}
}
