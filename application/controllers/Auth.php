<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
        parent::__construct();

        $this->load->model('model_admin');
    }

	public function index()
	{
		$data['title'] = 'KPI-Rektor | Login';
		$this->load->view('login',$data);
	}

	public function login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) {
	        $username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($data = $this->model_admin->select($username,$password)) {
				$userdata = array(
				    'username'     => $data['username'],
				    'name'		=> $data['name'],
				    'password' => $data['password'],
                    'id_jabatan' => $data['id_jabatan'],
				    'logged_in' => TRUE
				);
				$this->session->set_userdata($userdata);

				redirect('/');
			} else {
				redirect('auth');
			}
        } /*else echo 'salah input';
        echo $email."".$password;*/
	}

	public function logout() {
        // removing session data
        $userdata = array(
				    'username'     => null,
				    'name'		=> null,
						'password' => null,
				    'logged_in' => false
				);

        $this->session->unset_userdata($userdata);
        $this->session->sess_destroy();

        redirect('auth');
    }
}
