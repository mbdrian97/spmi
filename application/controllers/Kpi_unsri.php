<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kpi_unsri extends CI_Controller {

	function __construct(){
		parent::__construct();
		/* Check Session */
		if(!$this->session->userdata('logged_in')) redirect('auth');
		$this->load->model('model_kategori_kpi');
		$this->load->model('model_jabatan');
		$this->load->model('model_kpi');
		$this->load->model('model_kpi_dtt');
		$this->load->model('model_atasan_bawahan');
	}

	public function index() {
		$data['title'] = 'KPI-Rektor | Entry';
		$data['page'] = 'KPI-Unsri';
		$data['kpi'] = $this->model_kpi->select();
		$data['kategori'] = $this->model_kategori_kpi->select();
		$data['jabatan'] = $this->model_jabatan->select();

		$this->load->view('header',$data);
		$this->load->view('kpiunsri');
		$this->load->view('footer');
	}

	public function detail($id) {
		$data['title'] = 'KPI Rektor Unsri';
		$data['id'] = $this->model_jabatan->select($id);
		$data['page'] = $data['id']['deskripsi_jabatan'];

		$data['dtt_kpi'] = $this->model_kpi_dtt->selectkpiatasan($id);

		$data['kpi_dtt_tahunan'] = array();
		$data['total'] = "-";
		$data['tahun'] = date('Y');
		if (isset($_POST['tahun_kpi'])) {
			$data['tahun'] = $_POST['tahun_kpi'];
			$data['kpi_dtt_tahunan'] = $this->model_kpi_dtt->selectdetail($id,$this->input->post('tahun_kpi'));
			$data['total'] = 0;
			foreach ($data['kpi_dtt_tahunan'] as $kpi_dtt_tahunan) {
				$data['total'] += $kpi_dtt_tahunan['persentase_capaian'];
			}
		}

		$this->load->view('header',$data);
		$this->load->view('kpidetail');
		$this->load->view('footer');
	}

	public function update(){
		$this->form_validation->set_rules('id','Id','required');
		$this->form_validation->set_rules('baseline','Baseline','required');
		$this->form_validation->set_rules('target','Target','required');
		$this->form_validation->set_rules('capaian','Capaian','required');
		$this->form_validation->set_rules('bobot','Bobot','required');

		$id = $this->input->post('id');
		$capaian = $this->input->post('capaian');	
		$data = array(
			'baseline' => $this->input->post('baseline'),
			'target' => $this->input->post('target'),
			'bobot' => $this->input->post('bobot')
			);	
		if ($this->form_validation->run()) {
			$this->model_kpi_dtt->update($id,$data);
			if ($this->updatePersentase($id,$capaian)) {
				echo "<script type='text/javascript'>alert('Data berhasil diperbarui'); 
				window.history.go(-2);</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal diperbarui. Coba lagi'); 
				window.history.go(-2);</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong'); 
				window.history.go(-2);</script>";
		}
	}

	public function updatePersentase($id,$capaian_temp){
		$jabatan = $this->model_jabatan->getIdJabatan('Rektor');
		$data = $this->model_kpi_dtt->getRow($id);
		$id_jabatan = $data['id_jabatan'];
		$agr = $capaian_temp - $data['capaian'];
		while($id_jabatan!=$jabatan['id_jabatan']){
			$temp = $this->model_kpi_dtt->getRow($id);
			$capaian = $agr + $temp['capaian'];
			$persentase = number_format(round(($capaian/$temp['target']) * $temp['bobot'] * 100,2),2);
			$data = array(
				'capaian' => $capaian,
				'persentase_capaian' => floatval($persentase)
			);
			$this->model_kpi_dtt->update($id,$data);			

			$temp_kpi_dtt_tahunan = $this->model_kpi_dtt->selectdetail($id_jabatan,date('Y'));
			$total = 0;
			foreach ($temp_kpi_dtt_tahunan as $kpi_dtt_tahunan) {
				$total += $kpi_dtt_tahunan['capaian']*$kpi_dtt_tahunan['bobot'];
			}
			$current_month = date('m');

			$data = array(
				'total_capaian' => $total,
			);
			$this->model_kpi_dtt->setCapaianPerBulan($id_jabatan,$current_month, $data);

			$id_temp = $this->model_atasan_bawahan->getAtasan($id_jabatan);
			$id_jabatan = $id_temp['id_dtt_atasan'];
			$temp_id_kpp = $this->model_kpi_dtt->getRowByIdJabatanKPI($temp['id_kpi'],$id_jabatan);
			$id = $temp_id_kpp['id_kpi_dtt'];
		}	
		if($id_jabatan==$jabatan['id_jabatan']){
				$temp = $this->model_kpi_dtt->getRow($id);
				$capaian = $agr + $temp['capaian'];
				$persentase = number_format(round(($capaian/$temp['target']) * $temp['bobot'] * 100,2),2);
				$data = array(
					'capaian' => $capaian,
					'persentase_capaian' => floatval($persentase)
				);
				$this->model_kpi_dtt->update($id,$data);

				$temp_kpi_dtt_tahunan = $this->model_kpi_dtt->selectdetail($id_jabatan,date('Y'));
				$total = 0;
				foreach ($temp_kpi_dtt_tahunan as $kpi_dtt_tahunan) {
					$total += $kpi_dtt_tahunan['capaian']*$kpi_dtt_tahunan['bobot'];
				}
				$current_month = date('m');

				$data = array(
					'total_capaian' => $total,
				);
				$this->model_kpi_dtt->setCapaianPerBulan($id_jabatan,$current_month, $data);
			}
		return true;
	}

	public function add_kpi(){
		$this->form_validation->set_rules('kpi','Kpi', 'required');
		$this->form_validation->set_rules('jabatan','Jabatan', 'required');
		$kpi = $this->input->post('kpi');
		$id = $this->input->post('jabatan');
		if(!empty($kpi)){
			for ($i=0; $i < count($kpi) ; $i++) { 
					$this->model_kpi_dtt->insert_kpi($id,$kpi[$i]);
			}
		}
		echo "<script type='text/javascript'>alert('Data berhasil diperbarui'); 
				window.history.go(-2);</script>";
	}

	public function submit() {
		$this->form_validation->set_rules('jabatan_dtt','Jabatan_dtt','required');
		$this->form_validation->set_rules('kpi_dtt','Kpi_dtt','required');
		$this->form_validation->set_rules('tahun_dtt','Tahun_dtt','required');
		$this->form_validation->set_rules('baseline_dtt','Baseline_dtt','required');
		$this->form_validation->set_rules('capain_dtt','Capain_dtt','required');
		$this->form_validation->set_rules('target_dtt','Target_dtt','required');
		$this->form_validation->set_rules('bobot_dtt','Bobot_dtt','required');

		$data = array(
			'id_jabatan' 					=> $this->input->post('jabatan_dtt'),
			'id_kpi' 							=> $this->input->post('kpi_dtt'),
			'tahun' 							=> $this->input->post('tahun_dtt'),
			'baseline' 						=> $this->input->post('baseline_dtt'),
			'capaian'							=> $this->input->post('capain_dtt'),
			'target'							=> $this->input->post('target_dtt'),
			'bobot'								=> $this->input->post('bobot_dtt'),
		);

		if (isset($_POST["resubmit"])) {
			$destination = 'kpi_unsri#form';
		} else {
			$destination = 'kpi_unsri';
		}

		if ($this->form_validation->run()) {
			if ($this->model_kpi_dtt->insert($data)) {
				echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
				window.location.href = '".site_url($destination)."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
				window.location.href = '".site_url($destination)."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url($destination)."';</script>";
		}
	}
}
