<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->userdata('logged_in')) redirect('auth');
        $this->load->model('model_admin');
        $this->load->model('model_jabatan');
    }

	public function index() {
		$data['title'] = 'KPI-Rektor | Admin';
		$data['page'] = 'Administrator';
		$data['admin'] = $this->model_admin->get_data();
        $data['jabatan'] = $this->model_jabatan->select();

		$this->load->view('header',$data);
		$this->load->view('admin');
		$this->load->view('footer');
	}

	public function submit() {
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('repassword','Repassword','required');

		if ($this->form_validation->run()) {
			if ($this->input->post('password')==$this->input->post('repassword')) {
				$data = array(
					'username' => $this->input->post('username'),
				 	'password' => $this->input->post('password'),
					'name' => $this->input->post('name')
				);
				if ($this->model_admin->insert($data)) {
					echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
					window.location.href = '".site_url('admin')."';</script>";
				}
				else {
					echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
					window.location.href = '".site_url('dosen')."';</script>";
				}
			} else {
				echo "<script type='text/javascript'>alert('Error! Password tidak sama');
					window.location.href = '".site_url('admin')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('admin')."';</script>";
		}
	}

	public function update() {
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('repassword','Repassword','required');
		$this->form_validation->set_rules('admin_pass','Admin_pass','required');

		if ($this->form_validation->run()) {
			if ($this->input->post('admin_pass')==$this->session->userdata('password')) {
				if ($this->input->post('password')==$this->input->post('repassword')) {
					$id = $this->input->post('id');
					$data = array(
						'username' => $this->input->post('username'),
					 	'password' => $this->input->post('password'),
						'name' => $this->input->post('name')
					);
					if ($this->model_admin->update($id,$data)) {
						echo "<script type='text/javascript'>alert('Data berhasil diubah');
						window.location.href = '".site_url('admin')."';</script>";
					}
					else {
						echo "<script type='text/javascript'>alert('Data gagal diubah. Coba lagi');
						window.location.href = '".site_url('dosen')."';</script>";
					}
				} else {
					echo "<script type='text/javascript'>alert('Error! Password tidak sama');
						window.location.href = '".site_url('admin')."';</script>";
				}
			} else {
				echo "<script type='text/javascript'>alert('Error! Password anda salah');
					window.location.href = '".site_url('admin')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('admin')."';</script>";
		}
	}
    
    public function addJabatan() {
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('id_jabatan','id_jabatan','required');

		if ($this->form_validation->run()) {
            $checkAdmin = $this->model_admin->select($this->input->post('username'));
			if ($checkAdmin) {
				$data = array(
				 	'id_jabatan' => $this->input->post('id_jabatan')
				);
				if ($this->model_admin->update($checkAdmin['id'], $data)) {
					echo "<script type='text/javascript'>alert('Data berhasil ditambahkan');
					window.location.href = '".site_url('admin')."';</script>";
				}
				else {
					echo "<script type='text/javascript'>alert('Data gagal ditambahkan. Coba lagi');
					window.location.href = '".site_url('dosen')."';</script>";
				}
			} else {
				echo "<script type='text/javascript'>alert('Error! Username tidak ditemukan');
					window.location.href = '".site_url('admin')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('admin')."';</script>";
		}
	}

	public function delete() {
		$this->form_validation->set_rules('id','Id','required');

		if ($this->form_validation->run()) {
			$id = $this->input->post('id');
			if ($this->model_admin->delete($id)) {
				echo "<script type='text/javascript'>alert('Data berhasil dihapus');
				window.location.href = '".site_url('admin')."';</script>";
			}
			else {
				echo "<script type='text/javascript'>alert('Data gagal dihapus. Coba lagi');
				window.location.href = '".site_url('dosen')."';</script>";
			}
		} else {
			echo "<script type='text/javascript'>alert('Error! Data tidak boleh kosong');
				window.location.href = '".site_url('admin')."';</script>";
		}
	}
}
