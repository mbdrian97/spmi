<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_atasan_bawahan extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'atasan_bawahan';
	}

	public function select() {
		$table = $this->get_table();

		$this->db->select('id_atasan_bawahan');
		$this->db->select('a.deskripsi_jabatan as atasan');
		$this->db->select('b.deskripsi_jabatan as bawahan');
		$this->db->from($table);
		$this->db->join('master_jabatan as a','a.id_jabatan=id_dtt_atasan');
		$this->db->join('master_jabatan as b','b.id_jabatan=id_dtt_bawahan');

		$result = $this->db->get();

		return $result->result_array();
	}

	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function update($id, $data) {
		$table = $this->get_table();

		$this->db->where('id_atasan_bawahan',$id);
		return $this->db->update($table,$data);
	}

	public function delete($id) {
		$table = $this->get_table();

		$this->db->where('id_atasan_bawahan',$id);
		return $this->db->delete($table);
	}

	public function getAtasan($id){
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id_dtt_bawahan',$id);
		$result = $this->db->get();

		return $result->row_array();
	}
}
