<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_kpi_dtt extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'kpi_dtt_tahunan';
	}

	public function getRow($id){
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
  		$this->db->where('id_kpi_dtt',$id);
		$result = $this->db->get();
		return $result->row_array();
	}

	public function getItemKPI($jabatan,$kategori){
		$table = $this->get_table();

		$this->db->select('count(*) as jumlah');
		$this->db->from($table);
		$this->db->join('master_kpi',$table.'.id_kpi=master_kpi.id_kpi');
		$this->db->join('master_kategori','master_kpi.id_kategori=master_kategori.id_kategori');
  		$this->db->where('id_jabatan',$jabatan);  		
		$result = $this->db->get();
		return $result->row_array();
	}

	public function getRowByIdJabatanKPI($id,$jabatan){
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
  		$this->db->where('id_kpi',$id);
		$this->db->where('id_jabatan',$jabatan);  		
		$result = $this->db->get();
		return $result->row_array();
	}

	public function select() {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
    $this->db->join('master_kategori',$table.'.id_kategori=master_kategori.id_kategori');

		$result = $this->db->get();

		return $result->result_array();
	}

	public function selectkpiatasan($id){
		$result = $this->db->query("SELECT kpi_dtt_tahunan.*,deskripsi_kpi FROM `kpi_dtt_tahunan` JOIN master_kpi ON (kpi_dtt_tahunan.id_kpi=master_kpi.id_kpi) where kpi_dtt_tahunan.id_jabatan IN (SELECT id_dtt_atasan FROM atasan_bawahan WHERE id_dtt_bawahan='$id') and kpi_dtt_tahunan.id_kpi NOT IN (SELECT id_kpi FROM kpi_dtt_tahunan WHERE id_jabatan ='$id')");

		return $result->result_array();
	}

	public function selectdetail($id, $year) {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
		$this->db->join('master_kpi',$table.'.id_kpi=master_kpi.id_kpi','left');
		$this->db->join('atasan_bawahan',$table.'.id_jabatan=id_dtt_bawahan','left');
		$this->db->where('id_jabatan',$id);
		$this->db->where('tahun',$year);
		$result = $this->db->get();

		return $result->result_array();
	}

	public function insert_kpi($id_jabatan,$id_kpi) {
		$query = $this->db->query("INSERT INTO kpi_dtt_tahunan (id_jabatan,id_kpi,tahun,baseline,capaian,target,bobot,rasio,persentase_capaian) SELECT '$id_jabatan',id_kpi,tahun,baseline,capaian,target,bobot,rasio,persentase_capaian FROM kpi_dtt_tahunan WHERE id_kpi_dtt='$id_kpi'");
	}

	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function update($id, $data) {
		$table = $this->get_table();

		$this->db->where('id_kpi_dtt',$id);
		return $this->db->update($table,$data);
	}

	public function delete($id) {
		$table = $this->get_table();

		$this->db->where('id_kpi',$id);
		return $this->db->delete($table);
	}

	public function setCapaianPerBulan($id_jabatan,$current_month, $data){
		$this->db->select('*');
		$this->db->from('table_capaian');
		$this->db->where('id_jabatan',$id_jabatan);
		$this->db->where('month(bulan)',$current_month);

		$query = $this->db->get();
		if (($query->row_array())!=0) {
			$this->db->where('id_jabatan',$id_jabatan);
			$this->db->where('month(bulan)',$current_month);
			return $this->db->update('table_capaian',$data);	
		} else {
			$data['id_jabatan'] = $id_jabatan;
			$data['bulan'] = date('Y-m-d');
			return $this->db->insert('table_capaian',$data);
		}
	}

	public function updateBobot($bobot,$id_kategori){
		return $this->db->query("UPDATE kpi_dtt_tahunan kp JOIN master_kpi mk ON mk.id_kpi=kp.id_kpi SET kp.bobot='$bobot' WHERE kp.id_jabatan=11 and mk.id_kategori=$id_kategori");
	}
}
