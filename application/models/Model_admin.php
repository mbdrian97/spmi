<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_admin extends CI_Model {
	private $user;
	private $password;
	private $table;

	private function get_table() {
		return $this->table = 'master_admin';
	}

	public function select($username=null, $password=null) {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
		
        if(!is_null($username)){
            $this->db->where('username',$username);
        }
        
        if(!is_null($password)){
            $this->db->where('password',$password);
        }

		$result = $this->db->get();

		return $result->row_array();
	}

	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function get_data() {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);

		$result = $this->db->get();
		return $result->result_array();
	}

	public function update($id, $data) {
		$table = $this->get_table();

		$this->db->where('id',$id);
		return $this->db->update($table,$data);
	}

	public function delete($id) {
		$table = $this->get_table();

		$this->db->where('id',$id);
		return $this->db->delete($table);
	}
}
