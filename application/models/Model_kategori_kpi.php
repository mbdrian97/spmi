<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_kategori_kpi extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'master_kategori';
	}

	public function select() {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
		$result = $this->db->get();

		return $result->result_array();
	}

	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function update($id, $data) {
		$table = $this->get_table();

		$this->db->where('id_jabatan',$id);
		return $this->db->update($table,$data);
	}

	public function delete($id) {
		$table = $this->get_table();

		$this->db->where('id_jabatan',$id);
		return $this->db->delete($table);
	}
}
