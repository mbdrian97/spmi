<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_dosen extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'master_dosen';
	} 

	public function select() {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->join('master_jabatan','master_dosen.id_jabatan=master_jabatan.id_jabatan','left');

		$this->db->from($table);

		$result = $this->db->get();

		return $result->result_array(); 
	}

	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function delete($id) {
		$table = $this->get_table();

		$this->db->where('id',$id);
		return $this->db->delete($table);
	}

	public function update($id, $data) {
		$table = $this->get_table();

		$this->db->where('id',$id);
		return $this->db->update($table,$data);
	}
}