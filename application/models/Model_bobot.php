<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bobot extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'bobot_kategori';
	}


	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function select($id_jabatan,$id_kategori) {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id_jabatan',$id_jabatan);
		$this->db->where('id_kategori',$id_kategori);
		$result = $this->db->get();

		return $result->row_array();
	}

	public function update($id, $data) {
		$table = $this->get_table();
		$this->db->where('id',$id);
		return $this->db->update($table,$data);
	}
	
}
