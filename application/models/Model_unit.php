<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_unit extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'unit_indikator';
	} 

	public function select() {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);

		$result = $this->db->get();

		return $result->row_array(); 
	}
}