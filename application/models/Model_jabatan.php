<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_jabatan extends CI_Model {
	private $table;

	private function get_table() {
		return $this->table = 'master_jabatan';
	}

	public function select($id=false) {
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);
		if ($id) {
			$this->db->where('id_jabatan',$id);
			$result = $this->db->get();

			return $result->row_array();
		} else {
			$result = $this->db->get();

			return $result->result_array();
		}
	}

	public function getIdJabatan($id){
		$table = $this->get_table();

		$this->db->select('*');
		$this->db->from($table);		
		$this->db->where('deskripsi_jabatan',$id);
		$result = $this->db->get();
		return $result->row_array();		
	}

	public function insert($data) {
		$table = $this->get_table();

		return $this->db->insert($table,$data);
	}

	public function update($id, $data) {
		$table = $this->get_table();

		$this->db->where('id_jabatan',$id);
		return $this->db->update($table,$data);
	}

	public function delete($id) {
		$table = $this->get_table();

		$this->db->where('id_jabatan',$id);
		return $this->db->delete($table);
	}
}
